#!/bin/bash
cpplint --filter=-whitespace/braces `find . -name "*.hpp" -print -or -name "*.cpp" -print`
