#include <signal.h>		// SIGINT
#include <iostream>

#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>

#include "useful_macros.hpp"
#include "network/NetworkServer.hpp"
#include "network/AppMessage/Serializer.hpp"

#include "common.hpp"

size_t handled, expected;
std::unique_ptr<NetworkServer> srv_ptr;
std::map<size_t, std::list<std::string> > res;

void interrupt_signal(int s)
{
  std::cerr << "interrupt catched, shutting down" << std::endl;

	srv_ptr->stop();
}

void sendMsg(clientId client_id, const std::string& s)
{
	size_t len = serialization::Serializer<std::string>::GetLength(s);

	NetworkMessage::SharedPtr net_msg( new NetworkMessage( len ) );

  uint8_t* buff = net_msg->GetBody();
	buff = serialization::Serializer<std::string>::Serialize(buff, s);

  //~ net_msg->printRaw();

	srv_ptr->sendMessage( client_id, std::move( net_msg ) );
}

class Notifier
{
  public:
    void NotifyMsg(clientId client_id, const NetworkMessage& msg);
    void NotifyConnection(clientId client_id);
    void NotifyDisconnection(clientId client_id);
};

void Notifier::NotifyMsg(clientId client_id, const NetworkMessage& msg)
{
	size_t id;
	std::string str;

	const uint8_t* buff = msg.GetBody();
	buff = serialization::Serializer<size_t>::Deserialize(buff, id);
	buff = serialization::Serializer<std::string>::Deserialize(buff, str);

	res[id].push_back(str);

  //printf("recieved from %lu: '%s' \n", id, str.c_str());
}

void Notifier::NotifyConnection(clientId client_id)
{
  //~ printf("Client %i connected\n", client_id);

  for ( const auto& str : server_msgs )
    sendMsg(client_id, str);
}

void Notifier::NotifyDisconnection(clientId client_id)
{
  //printf("Client %i disconnected\n", client_id);

  handled++;
  if ( expected <= handled ) {
    srv_ptr->stop();
  }
}

int main(int argc, char **argv)
{
	// parse program options

  std::string appName = boost::filesystem::basename( argv[0] ); 

	boost::program_options::options_description desc("Program options");
	desc.add_options()
    ("help,h", "Print help message")
    ("expected", boost::program_options::value<size_t>(&expected)->required(), "Expected number of clients") 
	;

  boost::program_options::positional_options_description positionalOptions; 
  positionalOptions.add("expected", 1);

	boost::program_options::variables_map vm;

	try
  {
    // throws on error
    boost::program_options::store(
      boost::program_options::command_line_parser(argc, argv)
      .options( desc )
      .positional( positionalOptions )
      .run(), vm);

    if (vm.count("help")) {
      std::cout << desc << "\n";
      return EXIT_SUCCESS;
    }

    // throws on error, so do after help, in case there are problems
    boost::program_options::notify( vm );
  }
  catch(boost::program_options::required_option& e)
  {
    std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
    std::cout << desc << "\n";
    return EXIT_FAILURE;
  }
  catch(boost::program_options::error& e) 
  {
    std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
    std::cout << desc << "\n";
    return EXIT_FAILURE;
  }

	handled = 0;

	srv_ptr = std::unique_ptr<NetworkServer>( new NetworkServer(PORT) );

	// subscribe a notifier
	Notifier notifier;
	srv_ptr->OnNewMessage().Subscribe(&notifier, &Notifier::NotifyMsg);
	srv_ptr->OnClientConnected().Subscribe(&notifier, &Notifier::NotifyConnection);
	srv_ptr->OnClientDisconnected().Subscribe(&notifier, &Notifier::NotifyDisconnection);

	// catch SIGINT signal to properly deactivate the sensor
	//signal(SIGABRT, &interrupt_signal);
	signal(SIGINT, &interrupt_signal);

	// start server
	srv_ptr->start();

  // Check results
  {
    assert( res.size() == expected );

    size_t i = 1;
    std::cout << "raw network test... " << std::endl;
    for( const auto& kv : res )
    {
      assert( kv.first == i );
      assert( kv.second == client_msgs );
      i++;
    }
    std::cout << "OK!" << std::endl;
  }

	return EXIT_SUCCESS;
}
