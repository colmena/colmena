
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>

#include <boost/asio/ip/tcp.hpp>

#include "ApplicationLog.hpp"
#include "Event/Event.hpp"
#include "network/ClientSocket.hpp"
#include "network/AppMessage/Serializer.hpp"

#include "common.hpp"

int id;
boost::asio::io_service io_service;
std::unique_ptr<ClientSocket> client_ptr;
std::list<std::string> recieved_msgs;

void kill_client(const boost::system::error_code& /*e*/)
{
  io_service.stop();
}

class Notifier
{
  public:

    void Notify(const NetworkMessage& msg);
};

void Notifier::Notify(const NetworkMessage& msg)
{
	std::string str;

  //~ msg.printRaw();

	const uint8_t* buff = msg.GetBody();
	buff = serialization::Serializer<std::string>::Deserialize(buff, str);

	recieved_msgs.push_back( str );

  //~ printf("recieved message from server: '%s' \n", str.c_str());
}

void sendMsg(const std::string& s)
{
	size_t len = serialization::Serializer<std::string>::GetLength(s) + serialization::Serializer<size_t>::GetLength(id);

	// Convert to NetworkMessage and send
	NetworkMessage::SharedPtr net_msg(new NetworkMessage( len ));

	uint8_t* buff = net_msg->GetBody();
	buff = serialization::Serializer<size_t>::Serialize(buff, id);
	buff = serialization::Serializer<std::string>::Serialize(buff, s);

	client_ptr->send( net_msg );
}

int main(int argc, char **argv)
{
	// parse program options

  std::string appName = boost::filesystem::basename( argv[0] ); 

	boost::program_options::options_description desc("Allowed options");
	desc.add_options()
    ("help,h", "Print help message")
    ("id", boost::program_options::value<int>(&id)->required(), "Client id")
	;

  boost::program_options::positional_options_description positionalOptions; 
  positionalOptions.add("id", 1); 

	boost::program_options::variables_map vm;

	try
  {
    // throws on error
    boost::program_options::store(
      boost::program_options::command_line_parser(argc, argv)
      .options( desc )
      .positional( positionalOptions )
      .run(), vm);

    if (vm.count("help")) {
      std::cout << desc << "\n";
      return EXIT_SUCCESS;
    }

    // throws on error, so do after help, in case there are problems
    boost::program_options::notify( vm );
  }
  catch(boost::program_options::required_option& e)
  { 
    std::cerr << "ERROR: " << e.what() << std::endl << std::endl; 
    std::cout << desc << "\n";
    return EXIT_FAILURE; 
  } 
  catch(boost::program_options::error& e) 
  {
    std::cerr << "ERROR: " << e.what() << std::endl << std::endl; 
    std::cout << desc << "\n";
    return EXIT_FAILURE; 
  }

	// start client
  
  client_ptr = std::unique_ptr<ClientSocket>( new ClientSocket( io_service ) );
	client_ptr->connect( "localhost", PORT_STR );

	Notifier notifier;
	ScopedSubscription subscription = client_ptr->onNewMessage().Subscribe(&notifier, &Notifier::Notify);

  for ( const auto& str : client_msgs )
    sendMsg(str);

  // Set a timed callback to kill the client after 2 seconds.
  boost::asio::deadline_timer kill_timer(io_service, boost::posix_time::seconds(2));
  kill_timer.async_wait( kill_client );

  client_ptr->start();

  // Check that the recieved messages are the expected ones.
  if ( recieved_msgs != server_msgs )
    for ( const auto& msg : recieved_msgs )
      std::cout << msg << std::endl;
  assert( recieved_msgs == server_msgs );

	return 0;
}
