Test the raw networking layer. Fire up a multi-client server and some
clients. The clients shoot some messages at the server and expect
a reply.
