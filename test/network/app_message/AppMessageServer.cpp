#include "AppMessageServer.hpp"
#include "ApplicationLog.hpp"

void AppMessageDispatcher::dispatch( clientId client_id, const NetworkMessage& msg )
{
  std::map<msg_t, std::unique_ptr<IMessageFactory> >::iterator it =
    factories_.find( AppMessage::GetType( msg ) );

  // If no registered handler exists, print a warning message
  if ( it == factories_.end() )
    GEL_WARNING("No handler exists for recieved message type.");

  // dispatch message to all subscribed handlers
  (it->second)->dispatch( msg );
}
