#include "Event/Event.hpp"
#include "network/NetworkMessage.hpp"
#include "network/NetworkServer.hpp"  // clientId
#include "network/AppMessage/AppMessage.hpp"

/**
 * This class handles incoming network messages, translating them
 * to the appropiate application level messages and dispatching them
 * to the message-type specific subscribed handlers.
 */
class AppMessageDispatcher
{
  public:

    /**
     * @brief
     *   decode an incoming network message and dispatch it
     *   to the subscribed application level handlers.
     */
    void dispatch( clientId client_id, const NetworkMessage& msg );

    /**
     * @brief
     *   register a handler for a specific message type.
     *   MSG_T must provide the static MSG_T::GetType() method.
     */
    template<typename MSG_T>
    void registerHandler( boost::function<void(const MSG_T&)> callback );

  private:

    // helper classes
    class IMessageFactory
    {
      public:

        virtual ~IMessageFactory() {};

        virtual void dispatch( const NetworkMessage& msg ) = 0;
    };

    template< typename MSG_T >
    class MessageFactory : public AppMessageDispatcher::IMessageFactory
    {
      public:

        ~MessageFactory() {};

        inline void dispatch( const NetworkMessage& msg ) override
        { on_new_message_.Notify( MSG_T( msg ) ); }

        // Event on which we will dispatch incoming messages of the specific type.
        // TODO there should be a subscribe-only interface to the Event.
        Event<const MSG_T&>& onNewMessage()
        { return on_new_message_; }

      private:

        Event<const MSG_T&> on_new_message_;
    };

    std::map<msg_t, std::unique_ptr<IMessageFactory> > factories_;
};

template<typename MSG_T>
void AppMessageDispatcher::registerHandler( boost::function<void(const MSG_T&)> callback )
{
  // Try to create a new Factory for this type of message.
  // If the factory already exists, std::map will not replace it
  // and return the old one.
  auto ret = factories_.insert(
    std::pair< msg_t, std::unique_ptr<IMessageFactory> >(
      MSG_T::type, std::unique_ptr<IMessageFactory>( new MessageFactory<MSG_T>() )
    )
  );

  // get abstract interface for specific message type factory.
  IMessageFactory& aux = *((ret.first)->second);

  // cast to the factory type we know is right
  MessageFactory<MSG_T>& factory = ( MessageFactory<MSG_T>& ) aux;

  factory.onNewMessage().Subscribe( callback );
}
