#define PORT 8000
#define PORT_STR "8000"

// hex("hola?") = 68 6f 6c 61 3f 00
const std::list<std::string> client_msgs = {"hola", "que tal", "?"};
const std::list<std::string> server_msgs = {"holi", "muy bien", "!!"};
