#include "network/ClientSocket.hpp"

// common parameters for server and client
#include "common.hpp"
#include "StringMsg.hpp"

int main(int argc, char **argv)
{
  boost::asio::io_service io_service;

  ClientSocket client_socket( io_service );

  client_socket.connect( "localhost", PORT_STR );

  for ( const auto& str : client_msgs )
  {
    StringMsg app_msg( str );
    NetworkMessage::ConstSharedPtr net_msg( app_msg.Serialize() );
    client_socket.send( net_msg );
  }

  // blocking
  client_socket.start();

  return EXIT_SUCCESS;
}
