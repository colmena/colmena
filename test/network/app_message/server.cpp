
// Colmena library headers
#include "network/NetworkServer.hpp"
#include "AppMessageServer.hpp"

// common parameters for server and client
#include "common.hpp"
#include "StringMsg.hpp"

class Notifier
{
  public:

    void notify(const StringMsg& msg)
    { std::cout << "recieved message " << msg.GetText() << std::endl; }
};

int main(int argc, char **argv)
{
  NetworkServer net_srv( PORT );

  AppMessageDispatcher msg_dispatcher;

  net_srv.OnNewMessage().Subscribe( &msg_dispatcher, &AppMessageDispatcher::dispatch );

  Notifier notifier;

  // register a handler for string messages
  msg_dispatcher.registerHandler<StringMsg>( std::bind(&Notifier::notify, &notifier, std::placeholders::_1) );

  net_srv.start();

  return EXIT_SUCCESS;
}
