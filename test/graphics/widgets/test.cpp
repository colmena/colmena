#include "ApplicationLog.hpp"
#include "graphics/MainWindow.hpp"
#include "graphics/widgets/Box.hpp"
#include "graphics/widgets/Button.hpp"
#include "graphics/widgets/DrawingArea.hpp"
#include "graphics/widgets/background/BackgroundDecorator.hpp"

#define COLOR_GREY Color( 200, 200, 200 )
#define COLOR_BLUE Color( 100, 100, 255 )

void notifyClick()
{
  GEL_INFO("Send Button Clicked");
}

int main(int argc, char **argv)
{
  MainWindow window("colmena graphics test window", Size(640, 480));

  colmena::widgets::VBox main_vbox;
  window.add( &main_vbox );

  colmena::widgets::DrawingArea drawing_area;
  colmena::widgets::BackgroundDecorator drawing_area_bg( drawing_area, COLOR_BLUE );
  main_vbox.add( drawing_area_bg );

  colmena::widgets::VBox chat_vbox;
  // don't expand chat widget, use a fixed height for it
  main_vbox.add( chat_vbox, false );

  colmena::widgets::Button chat_send_btn( "send" );
  chat_send_btn.onClick().Subscribe( &notifyClick );
  colmena::widgets::BackgroundDecorator button_bg( chat_send_btn, COLOR_GREY );
  chat_vbox.add( button_bg, false );

  window.mainLoop();

  return EXIT_SUCCESS;
}
