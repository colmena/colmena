#include "graphics/MainWindow.hpp"
#include "graphics/resources/Text.hpp"
#include "graphics/resources/Image.hpp"
#include "graphics/resources/ResourceManager.hpp"
#include "graphics/display_objects/Sprite.hpp"
#include "graphics/display_objects/Spritesheet.hpp"
#include "graphics/display_objects/LayeredDisplayObjectContainer.hpp"
#include "graphics/input/KeyboardEvents.hpp"

#include "tiled/WorldObjects/entities/Entity.hpp"
#include "tiled/WorldObjects/entities/EntityController.cpp"

#include "ApplicationLog.hpp"

#define COLOR_GOLD Color(255, 215, 0)
#define COLOR_ORANGE Color(255, 165, 0)
#define COLOR_DARK_ORANGE Color(255, 140, 0)

class EntityView : public Spritesheet
{
  public:

    // TODO I can't make it const due to event subscriptions.
    EntityView(/*const */Entity& entity, const Image& spritesheet);

  private:

    /*const*/ Entity& entity_;

  // aux methods

    void move(const Direction& dir);

    void update();

    void changeOrientation(Direction dir);

    void changePosition(const Position2D& new_pos);
};

EntityView::EntityView(/*const*/ Entity& entity, const Image& spritesheet)
  : Spritesheet(spritesheet, Size(3, 4), entity.getPosition())
  , entity_( entity )
{
  update();
  entity_.OnMove().Subscribe( this, &EntityView::move );
}

void EntityView::move(const Direction& dir)
{
  update();
}

void EntityView::update()
{
  changeOrientation( entity_.GetOrientation() );
  changePosition( entity_.getPosition() );
}

void EntityView::changeOrientation(Direction dir)
{
  switch( dir )
  {
    case Direction::N:
      selectRow( 3 );
      break;
    case Direction::E:
      selectRow( 2 );
      break;
    case Direction::S:
      selectRow( 0 );
      break;
    case Direction::W:
      selectRow( 1 );
      break;
    default:
      GEL_ERROR("FUCK!");
      break;
  }
}

void EntityView::changePosition(const Position2D& new_pos)
{
  position_ = new_pos * 35;
}

int main(int argc, char **argv)
{
  MainWindow window("colmena graphics test window", Size(640, 480));

  // Add 3 window layers
	LayeredDisplayObjectContainer window_layers( 3 );
  window.add( &window_layers );

  // Add a backgroun image
  std::string name = "/home/tfischer/Images/arch-wall.png";
  Image img( name );
  Sprite sprite( img );
  window_layers.add( &sprite, 0 );

  // Add some text
  Text label_txt = ResourceManager::getText( "Player stats", "/usr/share/fonts/TTF/arial.ttf", 14, COLOR_GOLD );
  Sprite label( label_txt, Position2D(20, 20) );
  window_layers.add( &label, 1 );

  // Add an entity for a new character
  Entity avatar(0, Position2D(4, 4), Direction::S);

  // Add a character from a spritesheet
  std::string char_name = "/home/tfischer/proyectos-new/tests-colmena/tiled/resources/set/FemaleDefault.png";
  Image char_img( char_name );
  EntityView char_view( avatar, char_img );
  window_layers.add( &char_view, 1 );

  // setup some callbacks to change character orientation
  EntityController controller( avatar );

  window.mainLoop();

  return EXIT_SUCCESS;
}
