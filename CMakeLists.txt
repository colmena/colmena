cmake_minimum_required (VERSION 2.8.11)
project(Colmena)

# TODO: Release/debug builds
# http://stackoverflow.com/questions/8591762/ifdef-debug-with-cmake-independent-from-platform
# http://stackoverflow.com/questions/7079947/cmake-debug-crossbuild

#set( CMAKE_VERBOSE_MAKEFILE on )

add_definitions( -std=c++11 -Wall )

# Build the colmena libraries
add_subdirectory(src)

# Build the doxy documentation
add_subdirectory(doc)

# TODO add a flag to enable/disable test building
add_subdirectory(test)
