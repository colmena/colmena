/**
 * File:   WorldObject.cpp
 * Author: tfischer
 *
 * Colmena, a game engine, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "WorldObject.hpp"

const WorldObjectId_t WorldObject::getId() const
{
    return id_;
}

const Position2D& WorldObject::getPosition() const
{
    return position_;
}

WorldObject::WorldObject(WorldObjectId_t id, const Position2D& position)
    : id_(id), position_(position)
{}
