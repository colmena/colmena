/**
 * File:   Entity.hpp
 * Author: tfischer
 *
 * Colmena, a game engine, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef COLMENA_TILED_WORLDOBJECTS_ENTITIES_ENTITY_HPP_
#define COLMENA_TILED_WORLDOBJECTS_ENTITIES_ENTITY_HPP_

#include "../WorldObject.hpp"
#include "../../../math/Direction.hpp"
#include "../../../Event/Event.hpp"

class Entity : public WorldObject
{
  public:

    typedef std::string Action_t;

    // TODO hardcoded. Should be parametrized somewhere
    //~ enum State { STAND=0, WALK, ATTACK, DIE };
    //~ static const uint32_t N_STATES = 4;

    Entity(WorldObjectId_t id, const Position2D& position, const Direction& orientation);

    void Move(Direction dir);

    const Direction& GetOrientation() const;

    /** triggered when the entity stops moving */
    Event<>& OnStand();

    /** triggered when the entity starts moving */
    Event<const Direction&>& OnMove();

  private:

    Direction orientation_;

    Event<> on_stand_;

    Event<const Direction&> on_move_;
};

#endif  // COLMENA_TILED_WORLDOBJECTS_ENTITIES_ENTITY_HPP_
