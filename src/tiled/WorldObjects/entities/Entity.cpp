/**
 * File:   Entity.cpp
 * Author: tfischer
 *
 * Colmena, a game engine, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "Entity.hpp"

Entity::Entity(WorldObjectId_t id, const Position2D& position, const Direction& orientation)
  : WorldObject(id, position), orientation_(orientation)
{}

void Entity::Move(Direction dir)
{
  switch( dir )
  {
    case Direction::N:
    {
      position_ -= Position2D(0,1);
      break;
    }
    case Direction::E:
    {
      position_ += Position2D(1,0);
      break;
    }
    case Direction::S:
    {
      position_ += Position2D(0,1);
      break;
    }
    case Direction::W:
    {
      position_ -= Position2D(1,0);
      break;
    }
  }

  orientation_ = dir;

  on_move_.Notify( dir );
}

const Direction& Entity::GetOrientation() const
{
  return orientation_;
}

Event<const Direction&>& Entity::OnMove()
{
  return on_move_;
}

Event<>& Entity::OnStand()
{
  return on_stand_;
}
