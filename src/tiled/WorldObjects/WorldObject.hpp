/**
 * File:   WorldObject.hpp
 * Author: tfischer
 *
 * Colmena, a game engine, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef COLMENA_TILED_WORLDOBJECTS_WORLDOBJECT_HPP_
#define COLMENA_TILED_WORLDOBJECTS_WORLDOBJECT_HPP_

#include <stdint.h>
#include "../../math/Point2D.hpp"

// TODO should be called UID not Id
// TODO position should not be a part of it
typedef uint32_t WorldObjectId_t;

class WorldObject
{
    public:

        const WorldObjectId_t getId() const;

        const Position2D& getPosition() const;

    protected:

        WorldObject(WorldObjectId_t id, const Position2D& position);

        WorldObjectId_t id_;

        Position2D position_;
};

#endif // COLMENA_TILED_WORLDOBJECTS_WORLDOBJECT_HPP_
