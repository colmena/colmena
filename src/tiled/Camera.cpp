/**
 * File:   Camera.cpp
 * Author: tfischer
 *
 * Colmena, a game engine, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "Camera.hpp"

Camera::Camera(const Size& window_size, const Size& tilesize)
  : window_size_(window_size) ,tilesize_(tilesize), centered_tile_(Position2D::Null())
{
  update();
}

/*const Size& Camera::getTilesize() const
{
  return tilesize_;
}*/

void Camera::centerOn(const Position2D& tile_coordinates)
{
  centered_tile_ = tile_coordinates;

  update();
}

Rect Camera::getRange() const
{
  return tile_rect_;
}

const Position2D& Camera::getCenter() const
{
  return centered_tile_;
}

Position2D Camera::tileToScreen(const Position2D& tile_coordinates) const
{
  return tile_coordinates * tilesize_ - camera_offset_;
}

Position2D Camera::screenToTile(const Position2D& world_coordinates) const
{
  return (world_coordinates+1) % tilesize_;
}

void Camera::update()
{
  // free pixels on each direction around the center
  Position2D space_to_draw = ( window_size_ - tilesize_ ) / 2;

  // number of tiles to draw in each direction around the center
  Position2D tiles_to_draw = space_to_draw / tilesize_;

  // TODO check for sturation on max_tile
  Position2D min_tile = centered_tile_ - Position2D::Min( tiles_to_draw, centered_tile_ );
  Position2D max_tile = centered_tile_ + tiles_to_draw + 1;

  // TODO esto saturaaaa!!
  //Position2D area_on_world = camera_.tileToScreen( centered_tile_ ) - space_to_draw;

  tile_rect_ = Rect( min_tile, min_tile + max_tile );
  
  // camera_offset = centered_tile_ * tilesize_ + tilesize_ / 2 -  window_size_ / 2
  camera_offset_ = centered_tile_ * tilesize_ - ( window_size_ - tilesize_ ) / 2;
}
