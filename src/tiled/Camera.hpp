/**
 * File:   Camera.hpp
 * Author: tfischer
 *
 * Colmena, a game engine, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef COLMENA_TILED_CAMERA_HPP_
#define COLMENA_TILED_CAMERA_HPP_

#include "../math/Point2D.hpp"
#include "../math/Rect.hpp"

class Camera
{
  public:

    /**
     * @param window_size
     *   size of the captured window (in px).
     * 
     * @param tile_size
     *   size of a single tile (in px).
     */
    Camera(const Size& window_size, const Size& tile_size);

    /**
     * @return
     *   size of a single tile (in px).
     */
    //const Size& GetTilesize() const;

    /**
     * Center the camera on a certain tile
     */
    void centerOn(const Position2D& tile_coordinates);

    /**
     * Get the tile range that is displayed on the camera
     */
    Rect getRange() const;

    /**
     * @return
     *   the coordinates of the tile the camera is centered on.
     */
    const Position2D& getCenter() const;

    /**
     * Convert tile coordinates to world coordinates of its origin.
     */
    Position2D tileToScreen(const Position2D& tile_coordinates) const;

    /**
     * Convert world coordinates to the coordinates of the tile
     * where they are contained.
     */
    Position2D screenToTile(const Position2D& world_coordinates) const;

  private:

    Size window_size_;

    /** size of a single tile (in px) */
    Size tilesize_;

    /** coordinates of the tile the camera is centered on */
    Position2D centered_tile_;

    /** this variables are updated and stored for performance */
    /** rectangle depicting the visible screen tiles */
    Rect tile_rect_;
    /** camera offset (in px) on the pixel-sized map */
    Position2D camera_offset_;

    void update();
};

#endif /* COLMENA_TILED_CAMERA_HPP_ */
