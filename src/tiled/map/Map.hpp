/**
 * Generic Map model interface.
 * 
 * Author: tfischer
 *
 * Colmena, a game engine, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef COLMENA_TILED_MAP_MAP_HPP_
#define COLMENA_TILED_MAP_MAP_HPP_

#include <cstdint>
#include "../../math/Point2D.hpp"

typedef uint32_t TileId;

class Map
{
  public:

    virtual ~Map();

    /**
     * Get map width and height (in tiles)
     */
    virtual const Size& getSize() const = 0;

    /**
     * Get number of layers
     */
    virtual const uint32_t getNumLayers() const = 0;

    /**
     * Get tile Id at layer z and position (i,j)
     */
    virtual TileId getTile(int i, int j, int z) const = 0;

    /**
     * return true if tile at layer z and position (i,j) is walkable
     */
    virtual bool isWalkable(int i, int j, int z) const = 0;
};

#endif /* COLMENA_TILED_MAP_MAP_HPP_ */
