/**
 * Author: tfischer
 *
 * Colmena, a game engine, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef COLMENA_TILED_MAP_TILESET_HPP_
#define COLMENA_TILED_MAP_TILESET_HPP_

#include "Map.hpp"
#include "../../graphics/resources/Resource.hpp"

class Tileset
{
  public:

    virtual ~Tileset();

    virtual const Resource& getTileResource(TileId id) const = 0;

    virtual const Size& getTilesize() const = 0;
};

#endif  /* COLMENA_TILED_MAP_TILESET_HPP_ */

