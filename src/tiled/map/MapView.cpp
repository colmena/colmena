/**
 * File:   MapView.cpp
 * Author: tfischer
 *
 * Colmena, a game engine, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "MapView.hpp"

MapView::MapView(const Map& map, const Tileset& tileset, const Camera& camera)
  : map_(map), tileset_(tileset), camera_(camera)
{}

MapView::~MapView()
{}

void MapView::Render(DisplayArea& display_area)
{
  // TODO @author tfischer. the two variables below dont change if the windows 
  // size doesnt, and they shouldnt be recomputed each frame
  
  //~ // free pixels on each direction around the center
  //~ Position2D space_to_draw = (display_area.GetSize() - camera_.GetTilesize()) / 2;
  //~ // number of tiles to draw in each direction around the center
  //~ Position2D tiles_to_draw = space_to_draw / camera_.GetTilesize();
//~ 
  //~ // TODO check for sturation on max_tile
  //~ Position2D min_tile = camera_.GetCenter() - Position2D::Min( tiles_to_draw, camera_.GetCenter() );
  //~ Position2D max_tile = camera_.GetCenter() + tiles_to_draw + 1;
  Rect range = camera_.getRange();

  //~ GEL_DEBUG("(%u, %u) (%u, %u)", min_tile.GetY(), max_tile.GetY(), min_tile.GetX(), max_tile.GetX());
  forsn(i, range.GetY(), range.GetY() + range.GetHeight())
    forsn(j, range.GetX(), range.GetX() + range.GetWidth())
    {
      if ( 0 <= i && i < map_.getSize().GetY() && 0 <= j && j < map_.getSize().GetX() )
      {
        Position2D tile_on_screen = camera_.tileToScreen( Position2D(j, i) );

        // relative position to display area
        //Position2D position_on_area = tile_on_world - area_on_world;

        // TODO just drawing layer 0
        // draw resource to display area
        TileId tile = map_.getTile(i, j, 0);

        //~ GEL_DEBUG("draw tile (%u, %u): %u", j, i, tile);
        display_area.draw(tileset_.getTileResource(tile), tile_on_screen);
      }
    }
}
