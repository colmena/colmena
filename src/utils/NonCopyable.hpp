/* 
 * File:   NonCopyable.hpp
 * Author: tfischer
 *
 * Created on July 4, 2013, 2:17 PM
 */

#ifndef NONCOPYABLE_HPP
#define	NONCOPYABLE_HPP

#include <boost/noncopyable.hpp>

// TODO use C++11 " = delete";
typedef boost::noncopyable NonCopyable;

#endif	/* NONCOPYABLE_HPP */

