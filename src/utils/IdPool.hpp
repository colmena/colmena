/* 
 * File:   IdPool.hpp
 * Author: tfischer
 *
 * Created on July 4, 2013, 2:12 PM
 */

#ifndef IDPOOL_HPP
#define	IDPOOL_HPP

#include <limits>
#include "NonCopyable.hpp"

// TODO trivial implementation, id's are not reusable
template<class T>
class IdPool : public NonCopyable
{
  public:

    IdPool();

    // Allocates an id
    T getNext();

     // Frees an id so it can be used again
    void freeId(T id);

    //template<class U>
    //friend std::ostream& operator << (std::ostream& os, const IdPool<U>& pool);

  private:

    T next_;
};

template<class T>
IdPool<T>::IdPool()
{
  next_ = 1;
}

template<class T>
T IdPool<T>::getNext()
{
  return next_++;
}

template<class T>
void IdPool<T>::freeId(T id)
{}

/*template<class T>
std::ostream& operator << (std::ostream& os, const IdPool<T>& pool)
{
  return os << "[" << pool.next_ << "; " << std::numeric_limits<T>::max() << "]";
}*/

#endif	/* IDPOOL_HPP */
