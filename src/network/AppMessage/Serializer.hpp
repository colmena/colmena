/**
 * File:   Serializer.hpp
 * Author: tfischer
 *
 * Colmena, a game engine, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef COLMENA_NETWORK_APPMESSAGE_SERIALIZER_HPP_
#define COLMENA_NETWORK_APPMESSAGE_SERIALIZER_HPP_

#include <stdint.h>
#include <vector>
#include <ostream>
#include <string>

#include "../../useful_macros.hpp"

#define DEFINE_BASIC_TYPE_SERIALIZER(type) \
template<> \
struct Serializer<type>  \
{ \
  static uint8_t* Serialize(uint8_t* data, const type& n) \
  { \
    *((type*)data) = n; \
    return data+sizeof(type); \
  } \
  static const uint8_t* Deserialize(const uint8_t* data, type& n) \
  { \
    n = *((type*)data); \
    return data+sizeof(type); \
  } \
  static uint32_t GetLength(const type& n) \
  { \
    return sizeof(type); \
  } \
};

namespace serialization
{
    /** template declaration */

  template<class T>
  struct Serializer
  {
    /**
     * @brief Serializes the object to the location pointed by data.
     * @return Points to the first byte after the serialized data.
     */
    static uint8_t* Serialize(uint8_t* data, const T& obj);

    /**
     * @brief Deserializes the data to the object.
     * @return Points to the first byte after the data that was deserialized.
     */
    static const uint8_t* Deserialize(const uint8_t* data, T& obj);

    /**
     * @return The length of the serialized data.
     */
    static uint32_t GetLength(const T& obj);
  };

    /** Basic supported type specializations */

  DEFINE_BASIC_TYPE_SERIALIZER(int8_t)
  DEFINE_BASIC_TYPE_SERIALIZER(uint8_t)
  DEFINE_BASIC_TYPE_SERIALIZER(int16_t)
  DEFINE_BASIC_TYPE_SERIALIZER(uint16_t)
  DEFINE_BASIC_TYPE_SERIALIZER(int32_t)
  DEFINE_BASIC_TYPE_SERIALIZER(uint32_t)
  DEFINE_BASIC_TYPE_SERIALIZER(int64_t)
  DEFINE_BASIC_TYPE_SERIALIZER(uint64_t)

  //DEFINE_BASIC_TYPE_SERIALIZER(float32)
  //DEFINE_BASIC_TYPE_SERIALIZER(double64)

    /** std::string */

  template<>
  struct Serializer<std::string>
  {
    static uint8_t* Serialize(uint8_t* data, const std::string& str)
    {
      char* str_data = (char*)data;

      for( char c : str )
      {
        *str_data = c;
        str_data++;
      }

      *str_data = 0;
      str_data++;

      return (uint8_t*)str_data;
    }

    static const uint8_t* Deserialize(const uint8_t* data, std::string& str)
    {
      const char* str_data = (char*)data;

      while( *str_data != 0 )
      {
        str += *str_data;
        str_data++;
      }

      str_data++;

      return (uint8_t*)str_data;
    }

    static uint32_t GetLength(const std::string& str)
    {
      // return +1 for the null delimiting character
      return str.size() + 1;
    }
  };

    /** std::vectors */

  template<typename T>
  struct Serializer<std::vector<T> >
  {
    static uint8_t* Serialize(uint8_t* data, const std::vector<T>& v)
    {
      // serialize length
      data = Serializer<int32_t>::Serialize(data, (int32_t)v.size() );

      // serialize data
      for( T& elem : v )
        data = Serializer<T>::Serialize(data, elem);

      return data;
    }

    static const uint8_t* Deserialize(const uint8_t* data, std::vector<T>& v)
    {
      // deserialize length
      int32_t size;
      data = Serializer<int32_t>::Deserialize(data, size);

      v = std::vector<T>( size );

      // deserialize data
      forn(i, v.size())
        data = Serializer<T>::Deserialize(data, v[i]);

      return data;
    }

    static uint32_t GetLength(const std::vector<T>& v)
    {
      uint32_t length = Serializer<int32_t>::GetLength( (int32_t)v.size() );

      for( T& elem : v )
        length += Serializer<T>::GetLength( elem );

      return length;
    }
  };
}

template<class T>
std::ostream& operator<<(std::ostream& os, const std::vector<T>& v)
{
  os << "[";
    for( auto it=v.begin(); it!=v.end(); it++ )
    {
      if (it!=v.begin())
        os << " ,";
      os << *it;
    }
  return os << "]";
}

#endif  /* COLMENA_NETWORK_APPMESSAGE_SERIALIZER_HPP_ */
