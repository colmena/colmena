/**
 * File:   AppMessage.cpp
 * Author: tfischer
 *
 * Colmena, a game engine, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "AppMessage.hpp"
#include "Serializer.hpp"
#include "../../ApplicationLog.hpp"

// -----------------------------------------------------------------------------
// PUBLIC
// -----------------------------------------------------------------------------

/*final*/ NetworkMessage* AppMessage::Serialize() const
{
  NetworkMessage* msg = new NetworkMessage(GetLength());

  // GEL_DEBUG("Serializing %s of size %d+%d", GetType().c_str(), GetHeaderLength(), GetBodyLength());

  /** serialize header **/
  uint8_t* rest_of_data = SerializeHeader(msg->GetBody());

  /** serialize body (child class responsability) **/
  SerializeBody(rest_of_data);

  return msg;
}

/*final*/ void AppMessage::Deserialize(const NetworkMessage& msg)
{
  //_sender_sock_fd = sender_sock_fd;

  // GEL_DEBUG("Deserializing %s of size %d+%d", GetType().c_str(), GetHeaderLength(), GetBodyLength());

  /** deserialize heder */
  DeserializeHeader(msg.GetBody());

  /** deserialize body (child class responsability) */
  DeserializeBody(msg.GetBody() + GetHeaderLength());
}

/* final */ size_t AppMessage::GetLength() const
{
  return GetHeaderLength() + GetBodyLength();
}

std::ostream& operator << (std::ostream& os, const AppMessage& msg)
{
  os << "{";
  os << "  type: " << msg.GetType() << std::endl;
  msg.Print(os, 1);
  os << "}";
  return os;
}

msg_t AppMessage::GetType(const NetworkMessage& msg)
{
  std::string type;
  serialization::Serializer<msg_t>::Deserialize(msg.GetBody(), type);
  return type;
}

// -----------------------------------------------------------------------------
// PROTECTED
// -----------------------------------------------------------------------------

AppMessage::AppMessage()
{}

// -----------------------------------------------------------------------------
// PRIVATE
// -----------------------------------------------------------------------------

void AppMessage::DeserializeHeader(const uint8_t* data)
{
  // since the type is static, we have nothing to deserialize
}

uint8_t* AppMessage::SerializeHeader(uint8_t* buffer) const
{
  return serialization::Serializer<std::string>::Serialize(buffer, GetType());
}

size_t AppMessage::GetHeaderLength() const
{
  return serialization::Serializer<msg_t>::GetLength( GetType() );
}
