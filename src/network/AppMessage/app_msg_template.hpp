/*
 * @file:   <MSG_HEADER_FILE>
 * @author: <AUTHOR>
 *
 * Created on <DATETIME>
 */

#ifndef <MSG_DEFINE_NAME>
#define <MSG_DEFINE_NAME>

#include <vector>

// Colmena library headers
#include <Event/Event.hpp>
#include <network/AppMessage/AppMessage.hpp>
#include <useful_macros.hpp>
#include <ApplicationLog.hpp>
<MSG_EXTRA_LIB_HEADERS>

<MSG_EXTRA_HEADERS>

class <MSG_CLASS_NAME> : public AppMessage
{
  public:

    const static msg_t type;

    const static std::string name;

    <MSG_CLASS_NAME>(const NetworkMessage& msg);

    <MSG_CLASS_NAME>(<MSG_CLASS_PARAMETERS>);

<MSG_FIELD_GETTER_DECLARATION>

  protected:

    const msg_t& GetType() const
    { return type; }

    const std::string& GetName() const
    { return name; }

    size_t GetBodyLength() const;

    uint8_t* SerializeBody(uint8_t* data) const;

    const uint8_t* DeserializeBody(const uint8_t* msg);

    std::ostream& Print(std::ostream& os, size_t tabulation) const;

  private:

<MSG_CLASS_FIELDS>
};

#endif // <MSG_DEFINE_NAME>
