/*
 * File:   AppMessage.hpp
 * Author: tfischer
 *
 * Created on March 24, 2013, 5:00 PM
 */

#ifndef COLMENA_NETWORK_APPMESSAGE_APPMESSAGE_HPP_
#define	COLMENA_NETWORK_APPMESSAGE_APPMESSAGE_HPP_

#include <map>
#include <boost/multiprecision/cpp_int.hpp> // uint128_t

#include "../../Event/Event.hpp"
#include "../NetworkMessage.hpp"

/** The type of unique identifiers of messages types */
// TODO use boost::multiprecision::uint128_t and the md5 hash.
typedef std::string msg_t;

/**
 * Base class for the application messages.
 * It implements the header serialization/deserialization
 * which for now is only the type.
 *
 * TODO: add to header
 *   - magic tag
 *   - version number (format rules)
 */
class AppMessage
{
  public:

    typedef std::shared_ptr<AppMessage> SharedPtr;

    typedef std::shared_ptr<const AppMessage> ConstSharedPtr;

    /**
     * This function should not be overriden.
     * The serialization in the child classes is done
     * by implementing the virtual method
     * 'void SerializeeBody(uint8_t* data) const'
     * which is called from here.
     *
     * @return
     *   A ready-to-use NetworkMessage containing a serialized copy
     *   of the data from the application message.
     */
    /*final*/ NetworkMessage* Serialize() const;

    /**
     * This function should not be overriden.
     * The deserialization in the child classes is done automatically
     * by implementing the virtual method:
     * 'void DeserializeBody(uint8_t* data) const'
     * which is called from here.
     *
     * @param msg
     *   the network message to be deserialized into this object
     */
    /*final*/ void Deserialize(const NetworkMessage& msg);

    /**
     * This function should not be overriden.
     * returns the body length of the message. Essentially it returns
     * the length of the header implemented in this class,
     * added to the length of the body of the serialized data (child class),
     * retrieved through the virtual method
     * 'size_t GetBodyLength() const'.
     *
     * @return
     *   The size of the serialized message.
     */
    /* final */ size_t GetLength() const;

    /**
     * @brief Checks if the message has the given type
     * @param type The type to check for.
     * @return true if the type matches the one in the message, false otherwise.
     */
    //bool HasType(const msg_t& type) const;

    /**
     * Each child class should inform of their own UNIQUE! and DETERMINISTIC!
     * identifier through this function. On different builds of server
     * and client, the ids should be the same. For now we will take the string
     * of the message name.
     *
     * @return
     *   the unique identifier of the child class message.
     */
    virtual const msg_t& GetType() const = 0;

    /**
     * Deserialize the type of a serialized message
     *
     * @param msg
     *   the serialized message from which to retrieve the type
     * @return
     *   the type
     */
    static msg_t GetType(const NetworkMessage& msg);

    /**
     * ostream operator for debug purposes
     */
    friend std::ostream& operator << (std::ostream& os, const AppMessage& msg);

  protected:

    AppMessage();

    /**
     * Child classes should serialize their data to the pointer given
     * as parameter. The necessary space to do so will already be allocated
     * by this class before calling.
     *
     * @param buffer
     *   the buffer where the data should be serialized to
     * @return
     *   the number of bytes that were serialized
     */
    virtual uint8_t* SerializeBody(uint8_t* buffer) const = 0;

    /**
     * Child messages should deserialize their data taken from the pointer
     * given as parameter when called. It is used to reconstruct
     * a given message when a NetworkMessage arrives.
     *
     * @param data
     *   the pointer to the data to be deserialized.
     */
    virtual const uint8_t* DeserializeBody(const uint8_t* data) = 0;

    /**
     * Child classes should inform about their serialized body length here,
     * since it will be needed for this class to allocate the necessary space.
     *
     * @return
     *   the length of the body of the message
     */
    virtual size_t GetBodyLength() const = 0;

    /**
     * The child class message name.
     *
     * @return
     *   the declarative name of the child class message.
     */
    virtual const std::string& GetName() const = 0;

    /**
     * Child classes should implement this for debug purposes.
     *
     * @param os
     *   the stream where to print the message onto.
     * @param tabulation
     *   the tabulation that the output shoul prepend,
     *   to have a nice indented output.
     * @return
     *   the stream where the message was printed onto.
     */
    virtual std::ostream& Print(std::ostream& os, size_t tabulation) const = 0;

  private:

    /**
     * Deserialize header from serialized data
     *
     * @param data
     *   the buffer with the serialized header
     */
    void DeserializeHeader(const uint8_t* data);

    /**
     * Serializes message header to buffer
     *
     * @param buffer
     *   The buffer to serialize to
     * @return
     *   the number of bytes that were serialized
     */
    uint8_t* SerializeHeader(uint8_t* buffer) const;

    /**
     * Get the length of the serialized header
     *
     * @return
     *   length of the serialized header
     */
    size_t GetHeaderLength() const;
};

#endif /* COLMENA_NETWORK_APPMESSAGE_APPMESSAGE_HPP_ */
