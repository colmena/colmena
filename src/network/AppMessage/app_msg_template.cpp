/*
 * @file:   <MSG_CPP_FILE>
 * @author: <AUTHOR>
 *
 * Created on <DATETIME>
 */

#include "<MSG_HEADER_FILE>"

// Colmena library headers
#include <network/AppMessage/Serializer.hpp>
#include <network/dispatchable/MessageHandler.hpp>

// =============================================================================
// PUBLIC
// =============================================================================

const msg_t <MSG_CLASS_NAME>::type = "<MSG_CLASS_NAME>";

const std::string <MSG_CLASS_NAME>::name = "<MSG_CLASS_NAME>";

<MSG_CLASS_NAME>::<MSG_CLASS_NAME>(const NetworkMessage& msg)
{
  Deserialize( msg );
}

<MSG_CLASS_NAME>::<MSG_CLASS_NAME>(<MSG_CLASS_PARAMETERS>)
  : <MSG_CLASS_PARAMETERS_ASSIGNEMENT>
{}

<MSG_FIELD_GETTER_IMPLEMENTATION>

// =============================================================================
// PROTECTED
// =============================================================================

size_t <MSG_CLASS_NAME>::GetBodyLength() const
{ return <MSG_BODY_LENGTH>; }

uint8_t* <MSG_CLASS_NAME>::SerializeBody(uint8_t* data) const
{
<MSG_MEMBER_SERIALIZATION>
  return data;
}

const uint8_t* <MSG_CLASS_NAME>::DeserializeBody(const uint8_t* data)
{
<MSG_MEMBER_DESERIALIZATION>
  return data;
}

std::ostream& <MSG_CLASS_NAME>::Print(std::ostream& os, size_t tabulation) const
{
  forn(i,tabulation) os << "  "; os << GetName() << std::endl;
  forn(i,tabulation) os << "  "; os << "{" << std::endl;
  tabulation++;
<MSG_PRINT_FIELDS>
  tabulation--;
  forn(i,tabulation) os << "  "; os << "}" << std::endl;

  return os;
}
