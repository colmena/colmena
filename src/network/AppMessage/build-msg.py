#!/usr/bin/python3

import re
import os
import sys
import hashlib
import datetime
import argparse

# field_list_t: [<type:str, name:str, size:str, print:str, ?declaration_file:str>]
# field_declarations_t: [<type:str,name:str>]

def camelToUpperCase(camel_str):
  s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', camel_str)
  return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).upper()

def underscoreToCamelCase(underscore_str):
  return ''.join( [w.title() for w in underscore_str.split('_')] )

def removeDuplicates(ls):
  return list(set(ls))

def getExtraLibHeaders(field_list):
  header_list = [ "#include <"+field['libheader']+">" for field in field_list if 'libheader' in field]
  return "\n".join(removeDuplicates(header_list))

def getExtraHeaders(field_list):
  header_list = [ "#include \""+field['header']+"\"" for field in field_list if 'header' in field]
  return "\n".join(removeDuplicates(header_list))

def getConstructorParameters(field_list):
  if len(field_list) > 0:
    return ", ".join([ field['type'] + " " + field['name'] for field in field_list ])
  else:
    # return stub parameter to diferentiate from other constructor
    return "int"

def getClassParameterTypes(field_list):
  return ", ".join([ field['type'] for field in field_list ])

def getClassParameterNames(field_list):
  return ", ".join([ "_"+field['name'] for field in field_list ])

def getConstructorParametersAssignement(field_list):
  return ", ".join([ "_" + field['name'] + "(" + field['name'] + ")" for field in field_list ])

def getFieldGetterDeclaration(field_list):
  return "\n\n".join([ "    const " + field['type'] + "& Get" + underscoreToCamelCase(field['name']) + "() const;" for field in field_list ])

def getFieldGetterImplementation(class_name, field_list):
  return "\n\n".join([ "const " + field['type'] + "& " + class_name + "::Get" + underscoreToCamelCase(field['name']) + "() const { return _" + field['name'] + "; }" for field in field_list ])

def getFieldDefinition(field_list):
  return "\n\n".join([ "    " + field['type'] + " _" + field['name'] + ";" for field in field_list ])

def getFieldPrint(field_list):
  return "\n".join([ "    forn(i,tabulation) os << \"  \"; os << " + field['print'] + " << std::endl;" for field in field_list ])

def getBodyLength(field_list):
  return " + ".join([ "serialization::Serializer<"+field['type']+" >::GetLength(_"+field['name']+")" for field in field_list ])

def getMemberSerialization(field_list):
  return "\n".join([ "  data = serialization::Serializer<"+field['type']+" >::Serialize(data, _"+field['name']+");" for field in field_list ])

def getMemberDeserialization(field_list):
  return "\n".join([ "  data = serialization::Serializer<"+field['type']+" >::Deserialize(data, _"+field['name']+");" for field in field_list ])

# @ param class_name: string
# @ param field_list: field_list_t
def buildReplaces(class_name, field_list):
  tokens = {
    # name of the file author // TODO: get computer username?
    "<AUTHOR>": "tfischer",
    # date and time of creation. format: September 4, 2012, 8:53 PM
    "<DATETIME>": datetime.datetime.now().strftime("%d/%m/%Y at %X"),
    # name of the header file
    "<MSG_HEADER_FILE>": class_name+".hpp",
    # extra header inclusion for custom types from libraries
    "<MSG_EXTRA_LIB_HEADERS>": getExtraLibHeaders(field_list),
    # extra header inclusion for custom types
    "<MSG_EXTRA_HEADERS>": getExtraHeaders(field_list),
    # name of the implementation file
    "<MSG_CPP_FILE>": class_name+".cpp",
    # name of the #define field in the header
    "<MSG_DEFINE_NAME>": camelToUpperCase(class_name)+"_HPP_",
    # actual name of the class
    "<MSG_CLASS_NAME>": class_name,
    # id used for identifying the type of a serialized message
    "<MSG_HASH_ID>": hashlib.md5( class_name.encode('utf-8') ).hexdigest(),
    # declaration of the getters for the particular fields
    "<MSG_FIELD_GETTER_DECLARATION>": getFieldGetterDeclaration(field_list),
    # implementation of the getters for the particular fields
    "<MSG_FIELD_GETTER_IMPLEMENTATION>": getFieldGetterImplementation(class_name, field_list),
    # parameters passed to class constructor
    "<MSG_CLASS_PARAMETERS>": getConstructorParameters(field_list),
    # comma separeted list of the parameter types
    "<MSG_CLASS_PARAMETER_TYPES>" : getClassParameterTypes(field_list),
    # comma separeted list of the parameter names
    "<MSG_CLASS_PARAMETER_NAMES>" : getClassParameterNames(field_list),
    # list constructor for parameters
    "<MSG_CLASS_PARAMETERS_ASSIGNEMENT>": getConstructorParametersAssignement(field_list),
    # private class field definition
    "<MSG_CLASS_FIELDS>": getFieldDefinition(field_list),
    # prints fields to an ostream
    "<MSG_PRINT_FIELDS>": getFieldPrint(field_list),
    # length of the msg body
    "<MSG_BODY_LENGTH>": getBodyLength(field_list),
    # serialization sequence of member fields
    "<MSG_MEMBER_SERIALIZATION>": getMemberSerialization(field_list),
    # deserialization sequence of member fields
    "<MSG_MEMBER_DESERIALIZATION>": getMemberDeserialization(field_list)
  }

  return tokens

def generateFile(template_filename, output_filename, tokens):
  # read template template_filename
  f = open(template_filename,'r')
  template = f.read()
  f.close()

  # replace tokens
  for key, value in tokens.items():
    template = template.replace(key, value)

  # save as output_filename
  f = open(output_filename,'w')
  f.write(template)
  f.close()

def buildFiles(build_path, class_name, tokens):
  script_directory = os.path.dirname(os.path.realpath(__file__))
  generateFile(script_directory+"/app_msg_template.hpp", build_path+"/"+class_name+".hpp", tokens)
  generateFile(script_directory+"/app_msg_template.cpp", build_path+"/"+class_name+".cpp", tokens)
  #generateFile(script_directory+"/msg_template.lua", build_path+"/"+class_name+".lua", tokens)

def parseForListType(type_token):
  if type_token[len(type_token)-2:] == "[]":
    return "std::vector<" + type_token[:len(type_token)-2] + ">"
  else:
    return type_token

# @return field_list_t
def parseInputLine(line):
  # split line into individual tokens
  tokens = line.split()

  if len(tokens) == 4:
    if tokens[0] == "custom":
      field_type = parseForListType(tokens[1])
      field_name = tokens[2]
      declaration_file = tokens[3]
      return {
        'type': field_type,
        'name': field_name,
        'print': "_"+field_name,
        'header': declaration_file
      }
    elif tokens[0] == "lib":
      field_type = parseForListType(tokens[1])
      field_name = tokens[2]
      declaration_file = tokens[3]
      return {
        'type': field_type,
        'name': field_name,
        'print': "_"+field_name,
        'libheader': declaration_file
      }
    else:
      print("Error: directive "+tokens[0]+" unknown.")

  elif len(tokens) == 2:
    
    field_type = parseForListType(tokens[0])
    field_name = tokens[1]

    basic_types = {
      "bool": "uint8_t",
      "int8": "int8_t",
      "int16": "int16_t",
      "int32": "int32_t",
      "int64": "int64_t",
      "uint8": "uint8_t",
      "uint16": "uint16_t",
      "uint32": "uint32_t",
      "uint64": "uint64_t",
      "float32": "float",
      "float64": "double",
      "string": "std::string"
    }

    # check if basic type
    if field_type in basic_types.keys():
      return {
        'type': basic_types[field_type],
        'name': field_name,
        'print': "_"+field_name,
      }
    else:
      print("Error: "+field_type+" is not a valid type.")

  else:
    print("Error: Unsupported msg line format.")
  # in the future we can check for more complex types, i.e. string or other msg types
  # what about typedefs like 'typedef unsigned int WorldObjectId_t', I would like to use WorldObjectId_t as msg type (keyword?)

# @param filename: string, file with the msg definition
# @return field_declarations: field_declarations_t
def parseInputFile(filepath):
  field_list = []
  # TODO tirar un mensaje decente si el archivo no existe
  f = open(filepath,'r')
  for line in f.readlines():
    field_token = parseInputLine(line)
    if field_token:
      field_list.append(field_token)
  f.close()
  
  return field_list

if __name__ == "__main__":

  # parse program options

  parser = argparse.ArgumentParser()
  parser.add_argument("filename", help="msg file to be parsed")
  parser.add_argument("-o", "--output", help="output directory for the generated .cpp and .hpp files", default="./")
  args = parser.parse_args()

  print("filename", args.filename)

  # prepare

  # get only the filename (with extension)
  basename = os.path.basename( args.filename )

  # get the filename without extension
  class_name = os.path.splitext(basename)[0]

  # parse and generate c++ class files
  exit
  field_list = parseInputFile( args.filename )
  tokens = buildReplaces(class_name, field_list)
  buildFiles(args.output, class_name, tokens)
