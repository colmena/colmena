/**
 * Author: tfischer
 *
 * Colmena, a game engine, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef COLMENA_NETWORK_NETWORK_SERVER_HPP_
#define COLMENA_NETWORK_NETWORK_SERVER_HPP_

#include <map>
//#include <ctime>
//#include <string>
#include <queue>

#include <boost/asio/read.hpp>
#include <boost/asio/write.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/asio/placeholders.hpp>
#include <boost/enable_shared_from_this.hpp>

#include "NetworkMessage.hpp"
#include "NetworkSocket.hpp"

#include "../Event/ConcurrentEvent.hpp"
#include "../useful_macros.hpp"

// TODO: @author tfischer. using native_handle_type is a bad idea! 
// just for fast testing, not sure if windows compatible.
// It is used because the server needs to know the client id when
// recieveing a NewMessage event, and it is currently not possible
// to subscribe a bounded handler.
// We nned to change the Event interface to accept bounded functors.
// The client doesnt have to know its id. when the server subscribes
// to the OnNewMessage event, it should subscribe the handler
// binded with the corresponding client id
typedef boost::asio::ip::tcp::socket::native_handle_type clientId;

/**
 * A Network Server that handles multiple client connections.
 * It asynchronbously dispatches client events like connection, disconnection and new message arrival.
 * Messages can be broadcasted or sent to individual clients.
 * Clients can be kicked from server.
 * TODO: code KickClient and analyze concurrency issues!!
 */
class NetworkServer
{
	private:

		class ClientSession : public NetworkSocket, public boost::enable_shared_from_this<ClientSession>
		{
			public:

				typedef boost::shared_ptr<ClientSession> SharedPtr;

        /**
         * This class takes ownership of the socket, and invalidates
         * the pointer given as parameter.
         * TODO finally a shared_ptr had to be used, because a bind
         * function in NetworkServer::waitForNewClient() required
         * the socket ptr to have move semantics.
         * It should be unique_ptr.
         */
				ClientSession(SharedSocketPtr socket);

        /**  */
				void stop() override;
		};
	
  public:

    NetworkServer(unsigned short port_num);

    ~NetworkServer();

    /** Starts the server */
    void start();

		/** Stops the server */
    void stop();

    /** Kicks a client from the server */
    //bool KickClient(clientId client);

    /** Sends a server message to a specific client */
    bool sendMessage(clientId client, NetworkMessage::ConstSharedPtr msg);

    /** Broadcasts a server message to all clients */
    void broadcastMessage(NetworkMessage::ConstSharedPtr msg);

    /** Notifies when a client message arrives */
    ConcurrentEvent<clientId, const NetworkMessage&>& OnNewMessage();

    /** Notifies when a new client connects */
    ConcurrentEvent<clientId>& OnClientConnected();

    /** Notifies when a client's connection is lost */
    ConcurrentEvent<clientId>& OnClientDisconnected();

  private:

    // The io_service object provides I/O services, such as sockets, that the server object will use. 
    boost::asio::io_service io_service_;

    boost::asio::ip::tcp::acceptor acceptor_;

		std::map<clientId, ClientSession::SharedPtr> clients_;

    ConcurrentEvent<clientId> on_client_connected_;
    ConcurrentEvent<clientId> on_client_disconnected_;
    ConcurrentEvent<clientId, const NetworkMessage&> on_new_msg_;

    void waitForNewClient();

    void handleNewClient(const SharedSocketPtr& pSocket, const boost::system::error_code& error);

		void handleNewMessage(clientId id, const NetworkMessage& msg);

    void handleClientDisconnected(clientId id);

    // for debugging purposes
    std::ostream& printClients(std::ostream& os) const;
};

#endif /* COLMENA_NETWORK_NETWORK_SERVER_HPP_ */
