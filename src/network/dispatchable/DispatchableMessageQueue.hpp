/**
 * File:   DispatchableMessageQueue.hpp
 * Author: tfischer
 *
 * Colmena, a game engine, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef COLMENA_NETWORK_DISPATCHABLE_DISPATCHABLE_MESSAGE_QUEUE_HPP_
#define COLMENA_NETWORK_DISPATCHABLE_DISPATCHABLE_MESSAGE_QUEUE_HPP_

#include "../../concurrency/ThreadSafeQueue.hpp"
#include "IDispatchableMessage.hpp"
#include "DispatchableMessage.hpp"

/**
 * This class acts as a message queue where messages are pushed into.
 * On request, the top message is dispatched to the appropiate handlers.
 * The handlers subscribe to the queue if they want to handle certain messages.
 *
 * The class actually hides a collection of queues, one for each message type.
 */
class DispatchableMessageQueue
{
  public:

    ~DispatchableMessageQueue();

    void push( IDispatchableMessage* msg );

    void dispatchNext();

    bool empty() const;

    /**
     * register a message dispatcher
     */
    //void registerDispatcher( MessageDispatcher* dispatcher );

  private:

    /** the pending message queue */
    ThreadSafeQueue< IDispatchableMessage* > _queue;

    /** the list of dispatchers */
    //std::list< MessageDispatcher* > _dispatchers;
};

#endif /* COLMENA_NETWORK_DISPATCHABLE_DISPATCHABLE_MESSAGE_QUEUE_HPP_ */
