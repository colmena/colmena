/**
 * File:   DispatchableMessageQueue.cpp
 * Author: tfischer
 *
 * Colmena, a game engine, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "DispatchableMessageQueue.hpp"

DispatchableMessageQueue::~DispatchableMessageQueue()
{
  // delete remaining messages
  while ( !_queue.empty() )
  {
    IDispatchableMessage* msg = _queue.pop();
    delete msg;
  }
}

void DispatchableMessageQueue::push(IDispatchableMessage* msg)
{
  _queue.push(msg);
}

void DispatchableMessageQueue::dispatchNext()
{
  // get the next message
  IDispatchableMessage* msg = _queue.pop();

  // the message calls the appropiate MessageDispatcher
  // who in turn dispatches the message to all subscribed handlers
  msg->dispatch();

  // we are done with the message, get rid of it
  delete msg;
}

bool DispatchableMessageQueue::empty() const
{
  return _queue.empty();
}

