/**
 * File:   IDispatchableMessage.hpp
 * Author: tfischer
 *
 * Colmena, a game engine, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef COLMENA_NETWORK_DISPATCHABLE_IDISPATCHABLE_MSG_HPP_
#define COLMENA_NETWORK_DISPATCHABLE_IDISPATCHABLE_MSG_HPP_

#include <memory> /* auto_ptr */

/**
 * public interface for dispatchable messages
 */
class IDispatchableMessage
{
  public:

    typedef std::auto_ptr<IDispatchableMessage> UniquePtr;

    virtual ~IDispatchableMessage() {}

    virtual void dispatch() = 0;
};

#endif /* COLMENA_NETWORK_DISPATCHABLE_IDISPATCHABLE_MSG_HPP_ */
