/**
 * File:   MessageHandler.hpp
 * Author: tfischer
 *
 * Colmena, a game engine, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef COLMENA_NETWORK_DISPATCHABLE_MESSAGE_HANDLER_HPP_
#define COLMENA_NETWORK_DISPATCHABLE_MESSAGE_HANDLER_HPP_

#include <set>
#include <typeinfo> /* typeid */
#include "../../useful_macros.hpp"
#include "../../ApplicationLog.hpp"

template< class MSG_T >
class MessageHandler
{
  public:

    virtual ~MessageHandler( void );

    /**
     * Handler interface.
     */
    virtual void handleMessage( MSG_T& msg ) = 0;

  protected:

    MessageHandler( void );
};

template< class MSG_T >
class MessageDispatcher
{
  public:

    void dispatch( MSG_T& msg );

    void registerHandler( MessageHandler< MSG_T >* handler );

    void unregisterHandler( MessageHandler< MSG_T >* handler );

    static MessageDispatcher& getInstance( void );

  private:

    std::set< MessageHandler< MSG_T >* > _handlers;
};

template< class MSG_T >
MessageHandler< MSG_T >::MessageHandler( void )
{
  GEL_DEBUG("registering handler for type %s", typeid(MSG_T).name());
  MessageDispatcher< MSG_T >::getInstance().registerHandler( this );
}

template< class MSG_T >
MessageHandler< MSG_T >::~MessageHandler( void )
{
  GEL_DEBUG("unregistering handler for type %s", typeid(MSG_T).name());
  MessageDispatcher< MSG_T >::getInstance().unregisterHandler( this );
}

template< class MSG_T >
void MessageDispatcher< MSG_T >::dispatch( MSG_T& msg )
{
  GEL_DEBUG("dispatching to %lu handlers", _handlers.size());
  for( auto handler : _handlers )
  {
    handler->handleMessage( msg );
  }
}

template< class MSG_T >
void MessageDispatcher< MSG_T >::registerHandler( MessageHandler< MSG_T >* handler )
{
  GEL_DEBUG("handler beein registered for type %s", typeid(MSG_T).name());
  _handlers.insert( handler );
}

template< class MSG_T >
void MessageDispatcher< MSG_T >::unregisterHandler( MessageHandler< MSG_T >* handler )
{
  GEL_DEBUG("handler beein unregistered for type %s", typeid(MSG_T).name());
  _handlers.erase( handler );
}

template< class MSG_T >
MessageDispatcher< MSG_T >& MessageDispatcher< MSG_T >::getInstance( void )
{
  static MessageDispatcher< MSG_T > instance;
  return instance;
}

#endif /* COLMENA_NETWORK_DISPATCHABLE_MESSAGE_HANDLER_HPP_ */
