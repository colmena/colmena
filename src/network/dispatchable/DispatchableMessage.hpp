/**
 * File:   DispatchableMessage.hpp
 * Author: tfischer
 *
 * Colmena, a game engine, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef COLMENA_NETWORK_DISPATCHABLE_DISPATCHABLE_MSG_HPP_
#define COLMENA_NETWORK_DISPATCHABLE_DISPATCHABLE_MSG_HPP_

#include <memory>
#include "IDispatchableMessage.hpp"
#include "MessageHandler.hpp"
#include "../../ApplicationLog.hpp"


/**
 * a message which you can dispatch, which calls all subscribed handlers
 */
template< class MSG_T >
class DispatchableMessage : public IDispatchableMessage
{
  public:

    /**
     * Constructor. This class takes complete ownership of the parameter message
     */
    DispatchableMessage( std::auto_ptr<MSG_T>& msg );

    void dispatch();

  private:

    std::auto_ptr<MSG_T> _msg;
};

template< class MSG_T >
DispatchableMessage< MSG_T >::DispatchableMessage( std::auto_ptr<MSG_T>& msg ) : _msg(msg)
{}

template< class MSG_T >
void DispatchableMessage< MSG_T >::dispatch()
{
  MessageDispatcher< MSG_T >::getInstance().dispatch( *_msg );
}

#endif /* COLMENA_NETWORK_DISPATCHABLE_DISPATCHABLE_MSG_HPP_ */
