/**
 * File:   NetworkServer.cpp
 * Author: tfischer
 * 
 * Colmena, a game engine, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "NetworkServer.hpp"
#include <boost/range/adaptor/map.hpp>

// =============================================================================
// PUBLIC
// =============================================================================

NetworkServer::NetworkServer()
{
    _new_client_subscription = _connection_server.OnNewClient().Subscribe(this, &NetworkServer::HandleClientConnected);
}

void NetworkServer::Start(int port)
{
    _connection_server.Start(port);
}

bool NetworkServer::KickClient(int client_sock_fd)
{
  RemoveClient(client_sock_fd);
}

bool NetworkServer::SendMessage(int client_sock_fd, NetworkMessage* msg)
{
  _clients_mutex.Acquire();
  std::map<int, ConnectedServerSocket*>::iterator client_it = _clients.find(client_sock_fd);
  bool found = client_it != _clients.end();
  _clients_mutex.Release();

  if ( !found )
    return false;

  // TODO acquire single client mutex
  return client_it->second->Send(msg);
}

void NetworkServer::BroadcastMessage(NetworkMessage* msg)
{
  /** TODO que pasa si hay un error en alguno de los send? */
  _clients_mutex.Acquire();
  for( auto client_session : clients_ | boost::adaptors::map_values )
  {
    // TODO acquire single client mutex? Maybe not necessary because client list is mutexed
    client_session->Send(msg);
  }
  _clients_mutex.Release();
}

Event<int, NetworkMessage*>& NetworkServer::OnNewMessage()
{
  return _on_new_message;
}

Event<int>& NetworkServer::OnClientConnected()
{
  return _on_client_connected;
}

Event<int>& NetworkServer::OnClientDisconnected()
{
  return _on_client_disconnected;
}

// =============================================================================
// PRIVATE
// =============================================================================

void NetworkServer::HandleClientConnected(int client_sock_fd)
{
  /** Create the socket handler class */
  ConnectedServerSocket* new_client_handler = new ConnectedServerSocket(client_sock_fd);

  /** Try insertion of new socket */
  _clients_mutex.Acquire();
  std::pair<std::map<int, ConnectedServerSocket*>::iterator, bool> ret = _clients.insert(std::pair<int, ConnectedServerSocket*>(client_sock_fd, new_client_handler));
  _clients_mutex.Release();

  /** if insertion failed, raise an error */
  if (!ret.second)
  {
    GEL_ERROR("Socket %d was already a registered client!", client_sock_fd);
    delete new_client_handler;
    return;
  }

  /** subscribe the hadlers for the client events */
  new_client_handler->OnNewMessage().SubscribeHandler(this, &NetworkServer::HandleNewMessage);
  new_client_handler->OnConnectionLost().SubscribeHandler(this, &NetworkServer::HandleClientDisconnected);

  /** Notify creation of a new client */
  _on_client_connected.Notify(client_sock_fd);

  /** Start client handler listener/dispatcher loop */
  GEL_DEBUG("Going to call start loop");
  new_client_handler->StartLoop();
}

void NetworkServer::HandleClientDisconnected(int client_sock_fd)
{
  GEL_DEBUG("Lost client %d", client_sock_fd);
  
  if ( RemoveClient(client_sock_fd) )
    _on_client_disconnected.Notify(client_sock_fd);
  else
    GEL_ERROR("Socket %d was not a registered client!", client_sock_fd);
}

void NetworkServer::HandleNewMessage(int client_sock_fd, NetworkMessage* network_msg)
{}

bool NetworkServer::RemoveClient(int client_sock_fd)
{
  ConnectedServerSocket* client_handler = NULL;

  _clients_mutex.Acquire();

  /** locate active client handler */
  std::map<int, ConnectedServerSocket*>::iterator client_it = _clients.find(client_sock_fd);

  if ( client_it!=_clients.end() )
  {
    /** save client handler for further cleanup */
    client_handler = client_it->second;
    /** erase client from active handler list */
    _clients.erase(client_it);
  }

  _clients_mutex.Release();

  if ( client_handler == NULL )
    return false;

  /** delete client handler */
  GEL_DEBUG("Deleting client sock %d", client_sock_fd);
  delete client_handler;

  return true;
}
