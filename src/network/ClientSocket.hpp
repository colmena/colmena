/**
 * File:   NetworkClient.hpp
 * Author: tfischer
 * 
 * Colmena, a game engine, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef COLMENA_NETWORK_CLIENT_SOCKET_HPP_
#define COLMENA_NETWORK_CLIENT_SOCKET_HPP_

#include <boost/asio/io_service.hpp>

#include "NetworkSocket.hpp"

/**
 * Network client that connects and listens to messages
 * from a single server source, once a connection has been established.
 */
class ClientSocket : public NetworkSocket
{
  public:

    ClientSocket(boost::asio::io_service& io_service);

    /** Launches connection to the server */
    void connect(const std::string& host, const std::string& port);

    /** Blocking. Starts listening to server messages and dispatching */
    void start() override;

    /** Unblock. Stop listening to server messages and dispatching */
    void stop() override;

  private:

    // The io_service object provides I/O services, such as sockets.
    boost::asio::io_service& io_service_;
};

#endif /* COLMENA_NETWORK_CLIENT_SOCKET_HPP_ */
