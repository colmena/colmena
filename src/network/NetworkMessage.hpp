/**
 * File:   NetworkMessage.hpp
 * Author: tfischer
 * 
 * Colmena, a game engine, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef COLMENA_NETWORK_NETWORK_MESSAGE_HPP_
#define COLMENA_NETWORK_NETWORK_MESSAGE_HPP_

#include <string.h>
#include <stdint.h>
#include <iostream>
#include <memory>				// std::unique_ptr

/**
 * Represents a Raw data message with a size and body
 * that came or is going trough a network
 */
class NetworkMessage
{
  public:

    typedef std::shared_ptr<NetworkMessage> SharedPtr;
		typedef std::shared_ptr<const NetworkMessage> ConstSharedPtr;

		//~ typedef std::unique_ptr<NetworkMessage> UniquePtr;
		//~ typedef std::unique_ptr<const NetworkMessage> ConstUniquePtr;

    /**
     * Recieves the raw header data, decodes it,
     * and encodes it into the local data
     */
    NetworkMessage(uint8_t* header_data);

    /**
     * Recieves the header data, copies it,
     * and encodes it into the local data
     */
    NetworkMessage(size_t body_length);

    /** Get the message length of the whole data block */
    size_t GetLength() const;

    /** Get the message lengrh of the body */
    static size_t GetHeaderLength();

    /** Get the message lengrh of the body */
    size_t GetBodyLength() const;

    /**
     * Exposes the whole data block ( header + body )
     * possibly to send it trough the network
     */
    const uint8_t* GetData() const;

    /**
     * Exposes the whole body data block
     * possibly for debug purposes
     */
    const uint8_t* GetHeader() const;
    
    /**
     * Exposes the whole body data block
     * possibly to buffer the incoming message body
     */
    uint8_t* GetBody();

    /**
     * Exposes the whole body data block
     * possibly to deserialize the incoming message body
     */
    const uint8_t* GetBody() const;
    
    /**
     * Copies body data to the data block
     */
    void EncodeBody(const uint8_t* body_data);

    /**
     * ostream operator for debug purposes
     */
    friend std::ostream& operator << (std::ostream& os, const NetworkMessage& msg);

    /** functions for debug purposes */
    void printRawHeader() const;
    void printRawBody() const;
    void printRaw() const;
    
  private:

    std::unique_ptr<uint8_t[]> data_;

    size_t body_length_;

    /** Decode the header info from a header data block */
    void DecodeHeader(const uint8_t* header_data);

    /** Encodes the header info into the data block */
    void EncodeHeader();

    /** For debug purposes */
    void printRawData(const uint8_t* data, size_t length) const;

  public:

    enum { HEADER_LENGTH = sizeof(body_length_) };
};

#endif /* COLMENA_NETWORK_NETWORK_MESSAGE_HPP_ */
