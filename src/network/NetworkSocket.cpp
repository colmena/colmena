/**
 * File:   NetworkSocket.cpp
 * Author: tfischer
 *
 * Colmena, a game engine, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <boost/asio/read.hpp>
#include <boost/asio/write.hpp>
#include <boost/asio/placeholders.hpp>

#include "NetworkSocket.hpp"
#include "../ApplicationLog.hpp"

// =============================================================================
//      PUBLIC
// =============================================================================

NetworkSocket::NetworkSocket(SharedSocketPtr socket)
  : socket_( socket )
{}

NetworkSocket::~NetworkSocket()
{
  // make sure the socket is stopped
  stop();
}

void NetworkSocket::start()
{
  waitForMsg();
}

void NetworkSocket::stop()
{
  boost::system::error_code error;

	// ensure that any pending operations on the socket
	// are properly cancelled and any buffers are flushed
	// prior to the call to socket.close
	socket_->shutdown(boost::asio::ip::tcp::socket::shutdown_both, error);

	if ( error ) {
		GEL_ERROR("shutting down socket on handle %d: %s", socket_->native_handle(), error.message().c_str());
  }

	socket_->close( error );

	if ( error ) {
		GEL_ERROR("closing socket on handle %d: %s", socket_->native_handle(), error.message().c_str());
  }
}

void NetworkSocket::send(NetworkMessage::ConstSharedPtr msg)
{
  // dispatch an async wait for a message
  dispatchMessage( msg );

  // send a message (blocking)
  /*boost::system::error_code error;

  boost::asio::write(
    socket_,
    boost::asio::buffer(msg->GetData(), msg->GetLength()),
    // Complete condition: only return when all data is written to the socket.
    boost::asio::transfer_all(),
    error
  );

  if ( error ) {
    GEL_ERROR("Error sendin msg to server.");
    std::cerr << error.message() << std::endl;
  }*/
}

Event<const NetworkMessage&>& NetworkSocket::onNewMessage()
{
  return on_new_msg_;
}

Event<>& NetworkSocket::onConnectionLost()
{
  return on_connection_lost_;
}

// =============================================================================
//      PRIVATE
// =============================================================================

void NetworkSocket::waitForMsg()
{
	//GEL_DEBUG("Client waiting");
	
	boost::asio::async_read(
		*socket_,
		boost::asio::buffer(msg_header_buffer_, NetworkMessage::GetHeaderLength()),
		boost::bind(&NetworkSocket::handleMsgHeader, this, boost::asio::placeholders::error)
	);
}

void NetworkSocket::handleConnectionLost()
{
  on_connection_lost_.Notify();
}

void NetworkSocket::handleMsgHeader(const boost::system::error_code& error)
{
	//GEL_DEBUG("Client handling new header");
	
	if ( error )
	{
		//GEL_DEBUG("Endpoint left, shutting down connection. error: %s", error.message().c_str());
    handleConnectionLost();
		return;
	}
  
	// TODO if a corrupt header arrives, the NetworkMessage constructor
  // will try to allocate a crazy amount of memory and throw a
  // std::bad_alloc exception. We need to handle that.
	NetworkMessage::SharedPtr new_msg( new NetworkMessage( msg_header_buffer_ ) );

	boost::asio::async_read(
		*socket_,
		boost::asio::buffer(new_msg->GetBody(), new_msg->GetBodyLength()),
		boost::bind(&NetworkSocket::handleMsgBody, this, new_msg, boost::asio::placeholders::error)
	);
}

void NetworkSocket::handleMsgBody(NetworkMessage::SharedPtr new_msg, const boost::system::error_code& error)
{
	//GEL_DEBUG("Client handling new body");
	
	if ( error )
	{
		GEL_ERROR("Endpoint left(?), shutting down connection. error: %s", error.message().c_str());
    handleConnectionLost();
		return;
	}

	on_new_msg_.Notify( *new_msg );

	waitForMsg();

	//GEL_DEBUG("Client finished handling msg");
}

void NetworkSocket::dispatchMessage(NetworkMessage::ConstSharedPtr msg)
{
  // This is not thread safe. send() should not be called from
  // multiple threads that could cause a race condition on the queue.
  pending_deliveries_.push( msg );

  // check if a dispatching sequence is already in progress.
  bool write_in_progress = 1 < pending_deliveries_.size();

  // if a dispatching sequence is in progress, don't start one,
  // it will dispatch this message automatically when the previous one
  // is completely sent.
	if ( write_in_progress )
    return;

  // if no dispatching sequence is in progress, start one,
  boost::asio::async_write(
    *socket_,
    boost::asio::buffer(msg->GetData(), msg->GetLength()),
    boost::bind(&NetworkSocket::handleWriteComplete, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred)
  );
}

void NetworkSocket::handleWriteComplete(const boost::system::error_code& error, const size_t bytesTransferred)
{
	if ( error )
	{
		GEL_ERROR("endpoint left(?), shutting down connection. error: %s", error.message().c_str());
    handleConnectionLost();
		return;
	}

	pending_deliveries_.pop();

	if ( not pending_deliveries_.empty() )
	{
		boost::asio::async_write(
			*socket_,
			boost::asio::buffer(pending_deliveries_.front()->GetData(), pending_deliveries_.front()->GetLength()),
			boost::bind(&NetworkSocket::handleWriteComplete, this, boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred)
		);
	}
}
