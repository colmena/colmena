/**
 * File:   NetworkMessage.cpp
 * Author: tfischer
 *
 * Colmena, a game engine, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "NetworkMessage.hpp"
#include "../ApplicationLog.hpp"
#include "../useful_macros.hpp"

// =====================================================================
//      PUBLIC
// =====================================================================

NetworkMessage::NetworkMessage(uint8_t* header_data)
//  : data_( NULL )
{
  DecodeHeader(header_data);

  data_ = std::unique_ptr<uint8_t[]>( new uint8_t[ GetLength() ] );
  
  // just to keep the serialized data complete, not necessary i think
  EncodeHeader();
}

NetworkMessage::NetworkMessage(size_t body_length)
  : /*data_(NULL), */body_length_(body_length)
{
  data_ = std::unique_ptr<uint8_t[]>( new uint8_t[ GetLength() ] );

  EncodeHeader();
}

size_t NetworkMessage::GetLength() const
{
    return body_length_ + HEADER_LENGTH;
}

size_t NetworkMessage::GetHeaderLength()
{
    return HEADER_LENGTH;
}

size_t NetworkMessage::GetBodyLength() const
{
    return body_length_;
}

const uint8_t* NetworkMessage::GetData() const
{
    return data_.get();
}

const uint8_t* NetworkMessage::GetHeader() const
{
  return data_.get();
}

uint8_t* NetworkMessage::GetBody()
{
    return data_.get() + HEADER_LENGTH;
}

const uint8_t* NetworkMessage::GetBody() const
{
  return data_.get() + HEADER_LENGTH;
}

void NetworkMessage::EncodeBody(const uint8_t* body_data)
{
    memcpy(data_.get()+HEADER_LENGTH, body_data, GetBodyLength());
}

void NetworkMessage::printRawHeader() const
{
  printf(" > header(%zu): ", GetHeaderLength());
  printRawData(GetHeader(), GetHeaderLength());
  printf("\n");
}

void NetworkMessage::printRawBody() const
{
  printf(" > body(%zu): ", GetBodyLength());
  printRawData(GetBody(), GetBodyLength());
  printf("\n");
}

void NetworkMessage::printRaw() const
{
  printf(" > raw msg(%zu): ", GetLength());
  printRawData(GetData(), GetLength());
  printf("\n");
}

std::ostream& operator << (std::ostream& os, const NetworkMessage& msg)
{
  os << "msg(" << msg.body_length_ << ")<";
  forn(i, msg.body_length_)
  {
    os << ((unsigned char*)msg.data_.get())[msg.GetHeaderLength()+i] << "-";
  }
  os << ">";
  return os;
}

// =====================================================================
//      PRIVATE
// =====================================================================

void NetworkMessage::DecodeHeader(const uint8_t* header_data)
{
  memcpy(&body_length_, header_data, sizeof(body_length_));
}

void NetworkMessage::EncodeHeader()
{
  memcpy(data_.get(), &body_length_, sizeof(body_length_));
}

void NetworkMessage::printRawData(const uint8_t* data, size_t length) const
{
  forn(i, length)
    printf("%02x ", data[i]);
}
