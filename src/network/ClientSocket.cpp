/**
 * File:   ClientSocket.cpp
 * Author: tfischer
 *
 * Colmena, a game engine, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "ClientSocket.hpp"
#include "../ApplicationLog.hpp"
#include <boost/asio/connect.hpp>

ClientSocket::ClientSocket(boost::asio::io_service& io_service)
  : NetworkSocket( SharedSocketPtr( new boost::asio::ip::tcp::tcp::socket( io_service ) ) )
  , io_service_(io_service)
{}

void ClientSocket::start()
{
  NetworkSocket::start();

  try
  {
    io_service_.run();
  }
  catch (std::exception& e)
  {
    std::cerr << e.what() << std::endl;
  }
}

void ClientSocket::stop()
{
  NetworkSocket::stop();

  io_service_.stop();
}

void ClientSocket::connect(const std::string& host, const std::string& port)
{
  boost::asio::ip::tcp::resolver resolver( io_service_ );
  boost::asio::ip::tcp::resolver::query query(host, port);

  boost::system::error_code error;
  boost::asio::ip::tcp::resolver::iterator endpoint_iterator = resolver.resolve(query, error);

  if ( error )
  {
    GEL_ERROR("connecting socket: %s", error.message().c_str());
    return;
  }

  boost::asio::connect( *socket_, endpoint_iterator );
}
