/**
 * File:   NetworkSocket.hpp
 * Author: tfischer
 * 
 * Colmena, a game engine, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef COLMENA_NETWORK_NETWORK_SOCKET_HPP_
#define COLMENA_NETWORK_NETWORK_SOCKET_HPP_

#include <queue>
#include <boost/asio/ip/tcp.hpp>

#include "../Event/Event.hpp"
#include "NetworkMessage.hpp"

// for legibility
typedef std::shared_ptr<boost::asio::ip::tcp::tcp::socket> SharedSocketPtr;
//typedef std::unique_ptr<boost::asio::ip::tcp::tcp::socket> SharedSocketPtr;

/**
 * Abstract base class with basic network messaging routines
 * like send / recieve and callback oriented interface.
 */
class NetworkSocket
{
  public:

    NetworkSocket(SharedSocketPtr socket);

    virtual ~NetworkSocket();

    /** Blocking. Starts listening to server messages and dispatching */
    virtual void start();

    /** Unblock. Stop listening to server messages and dispatching */
    virtual void stop();

    /** @brief Send a message to the endpoint. */
    void send(NetworkMessage::ConstSharedPtr msg);

    // NetworkMessage* BlockUntilNextMessage();

    /** @brief Notify when the endpoint sends a message. */
    Event<const NetworkMessage&>& onNewMessage();

    /** @brief Notify when the connection to the endpoint is lost. */
    Event<>& onConnectionLost();

  protected:

    // A pointer is needed, because the socket has to come
    // through the constructor and the copy constructor
    // of the socket is deleted.
    SharedSocketPtr socket_;

  private:

    Event<> on_connection_lost_;

    Event<const NetworkMessage&> on_new_msg_;

    // The queue is used together with the strand to sequentially
    // dispatch the messages whenever the socket is avaible.
    std::queue<NetworkMessage::ConstSharedPtr> pending_deliveries_;

    // This is a buffer to read the header data of an incoming msg.
    uint8_t msg_header_buffer_[ NetworkMessage::HEADER_LENGTH ];

    void waitForMsg();

    void handleConnectionLost();

    void handleMsgHeader(const boost::system::error_code& error);

    void handleMsgBody(NetworkMessage::SharedPtr new_msg, const boost::system::error_code& error);

    void dispatchMessage(NetworkMessage::ConstSharedPtr msg);

    void handleWriteComplete(const boost::system::error_code& error, const size_t bytesTransferred);
};

#endif /* COLMENA_NETWORK_NETWORK_SOCKET_HPP_ */
