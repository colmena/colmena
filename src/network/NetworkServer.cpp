/**
 * Author: tfischer
 *
 * Colmena, a game engine, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "NetworkServer.hpp"
#include "../ApplicationLog.hpp"

#include <boost/range/adaptor/map.hpp>

NetworkServer::NetworkServer(unsigned short port_num)
  : acceptor_(io_service_, boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), port_num))
{}

NetworkServer::~NetworkServer()
{
  stop();
}

void NetworkServer::start()
{
  waitForNewClient();
	
	try
  {
    io_service_.run();
  }
  catch (std::exception& e)
  {
    std::cerr << e.what() << std::endl;
  }
}

void NetworkServer::stop()
{
  io_service_.stop();
}

bool NetworkServer::sendMessage(clientId client, NetworkMessage::ConstSharedPtr msg)
{
	std::map<clientId, ClientSession::SharedPtr>::iterator client_it = clients_.find(client);
  bool found = client_it != clients_.end();

  if ( not found )
  {
		GEL_ERROR("client %d is not registered", client);
    return false;
  }

	client_it->second->send( std::move(msg) );

  return true;
}

void NetworkServer::broadcastMessage(NetworkMessage::ConstSharedPtr msg)
{
	for( auto client_session : clients_ | boost::adaptors::map_values )
    client_session->send( std::move(msg) );
}

ConcurrentEvent<clientId>& NetworkServer::OnClientConnected()
{
	return on_client_connected_;
}

ConcurrentEvent<clientId>& NetworkServer::OnClientDisconnected()
{
	return on_client_disconnected_;
}

ConcurrentEvent<clientId, const NetworkMessage&>& NetworkServer::OnNewMessage()
{
	return on_new_msg_;
}

void NetworkServer::waitForNewClient()
{
  // Boost requires a socket to which to bind the client.
  // It can't be unique because bind() stores a copy of the parameter.
  SharedSocketPtr pSocket(new boost::asio::ip::tcp::tcp::socket( io_service_ ));

	acceptor_.async_accept(
		*pSocket,
		boost::bind(&NetworkServer::handleNewClient, this, pSocket, boost::asio::placeholders::error)
	);
}

void NetworkServer::handleNewClient(const SharedSocketPtr& pSocket, const boost::system::error_code& error)
{
	if ( error )
	{
		GEL_ERROR("accepting client. msg: %s", error.message().c_str());
		return;
	}

	clientId new_id = pSocket->native_handle();

  ClientSession::SharedPtr new_session( new ClientSession(SharedSocketPtr(pSocket)) );

	GEL_INFO("accepted new client %d", new_id);

	std::pair<std::map<clientId, ClientSession::SharedPtr>::iterator, bool> ret =
		clients_.insert(std::pair<clientId, ClientSession::SharedPtr>(new_id, new_session));

	/** if insertion failed, raise an error */

  // TODO the second client is inserted. We should preserve the first one
	if ( not ret.second )
	{
		GEL_ERROR("Socket %d was already a registered client!", new_id);
		return;
	}

	new_session->onNewMessage().Subscribe(boost::bind(&NetworkServer::handleNewMessage, this, new_id, _1));
  new_session->onConnectionLost().Subscribe(boost::bind(&NetworkServer::handleClientDisconnected, this, new_id));

	new_session->start();

  on_client_connected_.Notify( new_id );

	waitForNewClient();
}

void NetworkServer::handleClientDisconnected(clientId id)
{
  if ( clients_.erase( id ) <= 0 )
  {
		GEL_WARNING("client %d was not found on server", id);
	}

  on_client_disconnected_.Notify( id );
}

void NetworkServer::handleNewMessage(clientId id, const NetworkMessage& msg)
{
	on_new_msg_.Notify(id, msg);
}

std::ostream& NetworkServer::printClients(std::ostream& os) const
{
  os << "[";
  for ( const auto& kv : clients_ )
    os << kv.first << ", ";
  os << "]";
  return os;
}

// =====================================================================

NetworkServer::ClientSession::ClientSession(SharedSocketPtr socket)
	: NetworkSocket( socket )
{}

void NetworkServer::ClientSession::stop()
{
	//GEL_DEBUG("Client %d shutdown", id_);

  NetworkSocket::stop();

	// causes the reference count drop to 0.
	// this  will cause the object to be free'ed using delete.
	shared_from_this().reset();
}
