/**
 * Author: tfischer
 *
 * Colmena, a game engine, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef COLMENA_MATH_POINT2D_HPP_
#define COLMENA_MATH_POINT2D_HPP_

#include <math.h>
#include <string.h> // memcpy
#include <iostream>

#include "../network/AppMessage/Serializer.hpp"

template<class T>
class Point2D_
{
    public:

        Point2D_();

        Point2D_(const T& value);

        Point2D_(const T& x, const T& y);

        double Length() const;

        const T& GetX() const;
        const T& GetY() const;

        Point2D_ FlipX() const;
        Point2D_ FlipY() const;

        static const Point2D_& Null();

        static Point2D_ Min(const Point2D_& p, const Point2D_& q);
        static Point2D_ Max(const Point2D_& p, const Point2D_& q);

        /* Operators */

        Point2D_ operator + (const Point2D_& b) const;
        void operator += (const Point2D_& b);
        Point2D_ operator + (const int n) const;
        void operator += (const int n);

        Point2D_ operator - () const;
        Point2D_ operator - (const Point2D_& b) const;
        void operator -= (const Point2D_& b);
        Point2D_ operator - (const T& n) const;
        void operator -= (const T& n);

        Point2D_ operator * (const Point2D_& b) const;
        void operator *= (const Point2D_& b);
        Point2D_ operator * (const T& n) const;
        void operator *= (const T& n);

        Point2D_ operator / (const Point2D_& b) const;
        void operator /= (const Point2D_& b);
        Point2D_ operator / (const T& n) const;
        void operator /= (const T& n);

        Point2D_ operator % (const Point2D_& b) const;
        void operator %= (const Point2D_& b);
        Point2D_ operator % (const T& n) const;
        void operator %= (const T& n);

        bool operator < (const Point2D_& b) const;
        bool operator <= (const Point2D_& b) const;
        bool operator > (const Point2D_& b) const;
        bool operator >= (const Point2D_& b) const;
        
        template<class U>
        friend std::ostream& operator << (std::ostream& os, const Point2D_<U>& v);

        friend class serialization::Serializer<Point2D_>;

    private:

        T _x, _y;

        static const Point2D_ _null;
};

template<class T>
Point2D_<T>::Point2D_() : _x(0), _y(0)
{}

template<class T>
Point2D_<T>::Point2D_(const T& value) : _x(value), _y(value)
{}

template<class T>
Point2D_<T>::Point2D_(const T& x, const T& y) : _x(x), _y(y)
{}

template<class T>
double Point2D_<T>::Length() const
{
    return sqrt( _x*_x + _y*_y );
}

template<class T>
const T& Point2D_<T>::GetX() const
{
    return _x;
}

template<class T>
const T& Point2D_<T>::GetY() const
{
    return _y;
}

template<class T>
Point2D_<T> Point2D_<T>::Min(const Point2D_<T>& p, const Point2D_<T>& q)
{
  return Point2D_<T>(std::min(p._x, q._x), std::min(p._y, q._y));
}

template<class T>
Point2D_<T> Point2D_<T>::Max(const Point2D_<T>& p, const Point2D_<T>& q)
{
  return Point2D_<T>(std::max(p._x, q._x), std::max(p._y, q._y));
}

template<class T>
Point2D_<T> Point2D_<T>::FlipX() const
{
  return Point2D_<T>(-_x, _y);
}

template<class T>
Point2D_<T> Point2D_<T>::FlipY() const
{
  return Point2D_<T>(_x, -_y);
}

template<class T>
const Point2D_<T>& Point2D_<T>::Null()
{
  return _null;
}

template<class T>
Point2D_<T> Point2D_<T>::operator + (const Point2D_<T>& b) const
{
    return Point2D_<T>(_x+b._x, _y+b._y);
}

template<class T>
void Point2D_<T>::operator += (const Point2D_<T>& b)
{
    _x += b._x;
    _y += b._y;
}

template<class T>
Point2D_<T> Point2D_<T>::operator + (const int32_t n) const
{
    return Point2D_<T>(_x+n, _y+n);
}

template<class T>
void Point2D_<T>::operator += (const int32_t n)
{
    _x += n;
    _y += n;
}

template<class T>
Point2D_<T> Point2D_<T>::operator - () const
{
  return Point2D_<T>(-_x, -_y);
}

template<class T>
Point2D_<T> Point2D_<T>::operator - (const Point2D_<T>& b) const
{
    return Point2D_<T>(_x-b._x, _y-b._y);
}

template<class T>
void Point2D_<T>::operator -= (const Point2D_<T>& b)
{
    _x -= b._x;
    _y -= b._y;
}

template<class T>
Point2D_<T> Point2D_<T>::operator - (const T& n) const
{
    return Point2D_<T>(_x-n, _y-n);
}

template<class T>
void Point2D_<T>::operator -= (const T& n)
{
    _x -= n;
    _y -= n;
}

template<class T>
Point2D_<T> Point2D_<T>::operator * (const Point2D_<T>& b) const
{
  return Point2D_<T>(_x*b._x, _y*b._y);
}

template<class T>
void Point2D_<T>::operator *= (const Point2D_<T>& b)
{
  _x *= b._x;
  _y *= b._y;
}

template<class T>
Point2D_<T> Point2D_<T>::operator * (const T& n) const
{
    return Point2D_<T>(_x*n, _y*n);
}

template<class T>
void Point2D_<T>::operator *= (const T& n)
{
    _x *= n;
    _y *= n;
}

template<class T>
Point2D_<T> Point2D_<T>::operator / (const Point2D_<T>& b) const
{
  return Point2D_<T>(_x/b._x, _y/b._y);
}

template<class T>
void Point2D_<T>::operator /= (const Point2D_<T>& b)
{
  _x /= b._x;
  _y /= b._y;
}

template<class T>
Point2D_<T> Point2D_<T>::operator / (const T& n) const
{
    return Point2D_<T>(_x/n, _y/n);
}

template<class T>
void Point2D_<T>::operator /= (const T& n)
{
    _x /= n;
    _y /= n;
}

template<class T>
Point2D_<T> Point2D_<T>::operator % (const Point2D_<T>& b) const
{
  return Point2D_<T>(_x%b._x, _y%b._y);
}

template<class T>
void Point2D_<T>::operator %= (const Point2D_<T>& b)
{
  _x %= b._x;
  _y %= b._y;
}

template<class T>
Point2D_<T> Point2D_<T>::operator % (const T& n) const
{
  return Point2D_<T>(_x%n, _y%n);
}

template<class T>
void Point2D_<T>::operator %= (const T& n)
{
  _x %= n;
  _y %= n;
}

template<class T>
bool Point2D_<T>::operator < (const Point2D_<T>& b) const
{
  return _x < b._x;
}

template<class T>
bool Point2D_<T>::operator <= (const Point2D_<T>& b) const
{
  return _x <= b._x;
}

template<class T>
bool Point2D_<T>::operator > (const Point2D_<T>& b) const
{
  return _x > b._x;
}

template<class T>
bool Point2D_<T>::operator >= (const Point2D_<T>& b) const
{
  return _x >= b._x;
}

template<class T>
const Point2D_<T> Point2D_<T>::_null = Point2D_<T>(0,0);

template<class T>
std::ostream& operator << (std::ostream& os, const Point2D_<T>& p)
{
    os << '(' << p._x << ',' << p._y << ')';
    return os;
}

  /** Point2D_<T> */

namespace serialization
{

template< class T > 
struct Serializer< Point2D_<T> >
{
  static uint8_t* Serialize(uint8_t* data, const Point2D_<T>& p) \
  {
    data = Serializer<T>::Serialize(data, p._x);
    data = Serializer<T>::Serialize(data, p._y);
    return data;
  }
  static const uint8_t* Deserialize(const uint8_t* data, Point2D_<T>& p) \
  {
    data = Serializer<T>::Deserialize(data, p._x);
    data = Serializer<T>::Deserialize(data, p._y);
    return data;
  }
  static uint32_t GetLength(const Point2D_<T>& p)
  {
    return Serializer<T>::GetLength(p._x) + Serializer<T>::GetLength(p._y);
  }
};

/* TODO: delete this if the above code works. define tests.
uint32_t Serializer< Point2D_<T> >::GetLength(const Point2D_<T>& p)
{
  return Serializer<T>::GetLength(p._x) + Serializer<T>::GetLength(p._y);
}

template<class T>
uint8_t* Serializer< Point2D_<T> >::Serialize(uint8_t* data, const Point2D_<T>& p)
{
  data = Serializer<T>::Serialize(data, p._x);
  data = Serializer<T>::Serialize(data, p._y);
  return data;
}

template<class T>
const uint8_t* Serializer< Point2D_<T> >::Deserialize(const uint8_t* data, Point2D_<T>& p)
{
  data = Serializer<T>::Deserialize(data, p._x);
  data = Serializer<T>::Deserialize(data, p._y);
  return data;
}
*/
}

// Useful aliases
typedef Point2D_<int8_t> Point2c;
typedef Point2D_<uint8_t> Point2b;
typedef Point2D_<int32_t> Point2i;
typedef Point2D_<uint32_t> Point2w;
typedef Point2D_<float_t> Point2f;
typedef Point2D_<double_t> Point2d;

typedef Point2i Point2D;
typedef Point2w Position2D;
typedef Point2w Size;

#endif // COLMENA_MATH_POINT2D_HPP_
