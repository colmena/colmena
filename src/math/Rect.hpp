/**
 * Author: tfischer
 *
 * Colmena, a game engine, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef COLMENA_MATH_RECT_HPP_
#define COLMENA_MATH_RECT_HPP_

#include "Point2D.hpp"

template <class POS_T, class SIZE_T>
class Rect_
{
  public:

    Rect_();

    Rect_(const Point2D_<POS_T>& position, const Point2D_<SIZE_T>& size);

    const Point2D_<POS_T>& GetPosition() const;

    const Point2D_<SIZE_T>& GetSize() const;

    const POS_T& GetX() const;

    const POS_T& GetY() const;

    const SIZE_T& GetWidth() const;

    const SIZE_T& GetHeight() const;

    bool Contains(const Point2D_<POS_T>& p) const;

    static Rect_ Intersection(const Rect_& r1, const Rect_& r2);

    template <class POS_U, class SIZE_U >
    friend std::ostream& operator << (std::ostream& os, const Rect_<POS_U, SIZE_U >& r);

  private:

    Point2D_<POS_T> _position;
    Point2D_<SIZE_T> _size;
};

template <class POS_T, class SIZE_T>
Rect_<POS_T, SIZE_T>::Rect_()
{}

template <class POS_T, class SIZE_T>
Rect_<POS_T, SIZE_T>::Rect_(const Point2D_<POS_T>& position, const Point2D_<SIZE_T>& size)
  : _position(position), _size(size)
{}

template <class POS_T, class SIZE_T>
const Point2D_<POS_T>& Rect_<POS_T, SIZE_T>::GetPosition() const
{
  return _position;
}

template <class POS_T, class SIZE_T>
const Point2D_<SIZE_T>& Rect_<POS_T, SIZE_T>::GetSize() const
{
  return _size;
}

template <class POS_T, class SIZE_T>
const POS_T& Rect_<POS_T, SIZE_T>::GetX() const
{
  return _position.GetX();
}

template <class POS_T, class SIZE_T>
const POS_T& Rect_<POS_T, SIZE_T>::GetY() const
{
  return _position.GetY();
}

template <class POS_T, class SIZE_T>
const SIZE_T& Rect_<POS_T, SIZE_T>::GetWidth() const
{
  return _size.GetX();
}

template <class POS_T, class SIZE_T>
const SIZE_T& Rect_<POS_T, SIZE_T>::GetHeight() const
{
  return _size.GetY();
}

template <class POS_T, class SIZE_T>
bool Rect_<POS_T, SIZE_T>::Contains(const Point2D_<POS_T>& p) const
{
  return _position.GetX() <= p.GetX() && p.GetX() < _position.GetX()+_size.GetX()
      && _position.GetY() <= p.GetY() && p.GetY() < _position.GetY()+_size.GetY();
}

template <class POS_T, class SIZE_T>
Rect_<POS_T, SIZE_T> Rect_<POS_T, SIZE_T>::Intersection(const Rect_<POS_T, SIZE_T>& r1, const Rect_<POS_T, SIZE_T>& r2)
{
  Point2D union_position = Point2D::Max(r1._position, r2._position);
  Point2D union_outer_corner = Point2D::Min(r1._position+r1._size, r2._position+r2._size);

  Point2D union_size =
    ( union_outer_corner.GetX() <= union_position.GetX() || union_outer_corner.GetY() <= union_position.GetY() )
      ? Point2D::Null()
      : union_outer_corner-union_position;

  return Rect_(union_position, union_size);
}

template <class POS_T, class SIZE_T>
std::ostream& operator << (std::ostream& os, const Rect_<POS_T, SIZE_T>& r)
{
  return os << "[p: " << r.GetPosition() << ", s: " << r.GetSize() << "]";
}

// Useful aliases
typedef Rect_<uint32_t, uint32_t> Rect;

#endif // COLMENA_MATH_RECT_HPP_

