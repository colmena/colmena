/**
 * File:   MainWindow.hpp
 * Author: tfischer
 *
 * Colmena, a game engine, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef COLMENA_GRAPHICS_MAINWINDOW_HPP_
#define COLMENA_GRAPHICS_MAINWINDOW_HPP_

#include <SFML/Graphics.hpp>

#include "DisplayArea.hpp"
#include "DisplayObjectContainer.hpp"

class MainWindow : public DisplayObjectContainer
{
  public:

    /**
     * Build a new window.
     *
     * @param size
     *   The size of the new window
     */
    MainWindow(const std::string& title, const Size& size);

    /**
     * The main loop blocks until the window closes. It will render
     * the window periodically with all the contained DisplayObject's.
     */
    void mainLoop();

  private:

    // SFML main window object
    sf::RenderWindow window_;

    // Top level display area, representing the whole canvas
    DisplayArea display_area_;

  // helper functions

    /**
     * Render all content on screen.
     */
    void render();

    /**
     * Transform game to real world coordinates.
     *
     * @param position
     *   position in game (tile) coordinates
     * @return
     *   same position in screen (px) coordinates
     */
    //Point2D getPositionOnScreen(const Point2D& position);
};

#endif  /* COLMENA_GRAPHICS_MAINWINDOW_HPP_ */
