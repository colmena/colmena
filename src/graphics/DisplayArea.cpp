/**
 * File:   DisplayArea.cpp
 * Author: tfischer
 *
 * Colmena, a game engine, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sfml_bridge.hpp"
#include "DisplayArea.hpp"
#include "resources/Resource.hpp"
#include "../ApplicationLog.hpp"

DisplayArea DisplayArea::getSubArea(const Rect& clip) const
{
  if (
    clip.GetPosition().GetX() < 0 ||
    clip.GetPosition().GetY() < 0 ||
    window_.getSize().x < clip.GetSize().GetX() + clip.GetPosition().GetX() ||
    window_.getSize().y < clip.GetSize().GetY() + clip.GetPosition().GetY()
  )
    GEL_ERROR("clip is not contained in the target area, undefined behaviour expected");

  Rect new_clip(area_.GetPosition()+clip.GetPosition() , clip.GetSize());

  return DisplayArea(window_, new_clip);
}

void DisplayArea::draw(const Resource& resource, const Position2D& position, Rect* clip)
{
  // TODO compute intersection to ensure we don't draw outside of the display area
  //Rect display_rect = Rect::Intersection( area_, Rect(area_.GetPosition()+position, display_size) );

  resource.blitTo(window_, area_.GetPosition()+position, clip);
}

DisplayArea::DisplayArea(sf::RenderWindow& window)
  : window_( window ), area_( Position2D::Null(), fromSFML( window.getSize() ) )
{}

DisplayArea::DisplayArea(sf::RenderWindow& window, const Rect& area)
  : window_( window ), area_( area )
{}
