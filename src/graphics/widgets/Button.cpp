#include "Button.hpp"
#include "../resources/ResourceManager.hpp"

namespace colmena {

namespace widgets {

Button::Button( const std::string& text )
  : label_( ResourceManager::getText( text, "/usr/share/fonts/TTF/arial.ttf", 14, Color(0,0,0) ) )
{}

void Button::render( DisplayArea& display_area ) const
{
  display_area.draw( label_, Position2D::Null() );
}

void Button::handleMousePress(const MouseEvent& mouse_event) const
{
  // TODO can't do because of constness
  on_click_.Notify();
}

} // namespace widget

} // namespace colmena
