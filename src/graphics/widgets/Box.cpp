#include "Box.hpp"
#include "../../ApplicationLog.hpp"

namespace colmena {

namespace widgets {

void VBox::add(const IWidget& widget, bool expand, bool fill)
{
  packed_widgets_.push_back( PackedWidget(widget, expand, fill) );
  packed_widgets_.back().child_height_cache = widget.minHeight();
}

void VBox::render( DisplayArea& display_area ) const
{
  size_t min_height = minHeight();

  const Size& canvas_size = display_area.getSize();

  // compute size avaible for expanding widgets
  size_t expand_size = canvas_size.GetY() - min_height;

  // compute number of expanding widgets
  size_t n_expandable = 0;
  {
    for ( const PackedWidget& packed_widget : packed_widgets_ )
      if ( packed_widget.expand() )
        n_expandable++;
  }

  // compute amount of space given to each expanding widget.
  // TODO what if expand_size % n_expandable != 0 ???
  size_t single_expand_size = 0 < n_expandable ? expand_size / n_expandable : 0;

  size_t height_offset = 0;
  for ( const PackedWidget& packed_widget : packed_widgets_ )
  {
    // for expandable widgets, add a height bonus to their avaible display area
    size_t height_bonus = packed_widget.expand() ? single_expand_size : 0;

    Position2D child_pos( 0, height_offset );
    Size child_size( canvas_size.GetX(), packed_widget.widget().minHeight() + height_bonus );
    DisplayArea child_display_area = display_area.getSubArea( Rect( child_pos, child_size ) );

    packed_widget.widget().render( child_display_area );

    packed_widget.child_height_cache = child_size.GetY();
    height_offset += child_size.GetY();
  }
}

void VBox::handleMousePress(const MouseEvent& mouse_event) const
{
  //~ GEL_WARNING("VBox::handleMousePress not implemented yet");

  // TODO the problem is getting widget size because
  // we have no reference to the parent
  size_t height_offset = 0;

  for ( const PackedWidget& packed_widget : packed_widgets_ )
    if ( mouse_event.getPosition().GetY() <= height_offset + packed_widget.child_height_cache ) {
      packed_widget.widget().handleMousePress( MouseEvent( mouse_event.getPosition() - Position2D(0, height_offset), mouse_event.getButton() ) );
      break;
    } else {
      height_offset += packed_widget.child_height_cache;
    }
}

size_t VBox::minWidth() const
{
  size_t min_width = 0;
  for ( const PackedWidget& packed_widget : packed_widgets_ )
    min_width += packed_widget.widget().minWidth();

  return min_width;
}

size_t VBox::minHeight() const
{
  size_t min_height = 0;
  for ( const PackedWidget& packed_widget : packed_widgets_ )
    min_height += packed_widget.widget().minHeight();

  return min_height;
}

} // namespace widget

} // namespace colmena
