#ifndef COLMENA_WIDGETS_BACKGROUND_DECORATOR_HPP_
#define	COLMENA_WIDGETS_BACKGROUND_DECORATOR_HPP_

#include "../IWidget.hpp"
#include "../../resources/ColorRect.hpp"

namespace colmena {

namespace widgets {

class BackgroundDecorator : public IWidget
{
  public:

    BackgroundDecorator( const IWidget& decorated_widget, const Color& color )
      : decorated_widget_( decorated_widget ), color_rect_( color )
    {}

  // IWidget interface implementation

    void render( DisplayArea& display_area ) const override
    {
      Rect area(Position2D::Null(), display_area.getSize());
      display_area.draw(color_rect_, Position2D::Null(), &area);

      decorated_widget_.render( display_area );
    }

    void handleMousePress(const MouseEvent& mouse_event) const override
    { decorated_widget_.handleMousePress( mouse_event ); }

    size_t minWidth() const override
    { return decorated_widget_.minWidth(); }

    size_t minHeight() const override
    { return decorated_widget_.minHeight(); }

  private:

    const IWidget& decorated_widget_;

    ColorRect color_rect_;
};

} // namespace widget

} // namespace colmena

#endif // COLMENA_WIDGETS_BACKGROUND_DECORATOR_HPP_
