#include "InputBox.hpp"
#include "../resources/ResourceManager.hpp"

namespace colmena {

namespace widgets {

InputBox::InputBox( const std::string& font_name, uint32_t font_size )
  : has_focus_( false ), text_( "" ), font_name_( font_name ), font_size_( font_size )
{}

void InputBox::render( DisplayArea& display_area ) const
{
  Text text = ResourceManager::getText( text_, font_name_, font_size_, Color(0, 0, 0) );
  // TODO clip for area size
  display_area.draw(text, Position2D::Null());
}

void InputBox::handleMousePress(const MouseEvent& mouse_event) const
{
  // TODO enable input for writing
}

size_t InputBox::minHeight() const
{
  return ResourceManager::getFont( font_name_ ).getLineHeight( font_size_ );
}

} // namespace widget

} // namespace colmena

