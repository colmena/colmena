#ifndef COLMENA_WIDGETS_IWIDGET_HPP_
#define COLMENA_WIDGETS_IWIDGET_HPP_

#include "../DisplayObject.hpp"

// forawrd declaration
class DisplayArea;

namespace colmena {

namespace widgets {

class IWidget : public DisplayObject
{
  public:

    virtual ~IWidget() {}

    virtual void render( DisplayArea& display_area ) const override = 0;

    // TODO should be called handleMouseEvent
    virtual void handleMousePress(const MouseEvent& mouse_event) const override = 0;

    // Used to get minimum display area height to render the widget
    virtual size_t minWidth() const = 0;

    // Used to get minimum display area height to render the widget
    virtual size_t minHeight() const = 0;
};

} // namespace widget

} // namespace colmena

#endif // COLMENA_WIDGETS_IWIDGET_HPP_
