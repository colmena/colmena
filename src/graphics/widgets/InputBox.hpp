#ifndef COLMENA_WIDGETS_INPUT_BOX_HPP_
#define COLMENA_WIDGETS_INPUT_BOX_HPP_

#include "IWidget.hpp"
#include "../resources/Font.hpp"

namespace colmena {

namespace widgets {

class InputBox : public IWidget
{
  public:

    InputBox( const std::string& font_name, uint32_t font_size );

  // IWidget interface implementation

    void render( DisplayArea& display_area ) const override;

    void handleMousePress(const MouseEvent& mouse_event) const override;

    size_t minWidth() const override
    { return 0; }

    size_t minHeight() const override;

  private:

    bool has_focus_;

    std::string text_;

    const std::string& font_name_;

    uint32_t font_size_;
};

} // namespace widget

} // namespace colmena

#endif // COLMENA_WIDGETS_INPUT_BOX_HPP_
