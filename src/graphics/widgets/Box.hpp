#ifndef COLMENA_WIDGETS_BOX_HPP_
#define COLMENA_WIDGETS_BOX_HPP_

#include "IWidget.hpp"

namespace colmena {

namespace widgets {

/**
 * @brief helper class to store relations
 * between a container and a child widget
 */
class PackedWidget
{
  public:

    PackedWidget(const IWidget& widget, bool expand, bool fill)
      : widget_( widget ), expand_( expand ), fill_( fill )
    {}

    const IWidget& widget() const
    { return widget_; }

    bool expand() const
    { return expand_; }

    bool fill() const
    { return fill_; }

    // Ugly but necessary
    mutable size_t child_height_cache;

  private:

    // The widget to be packed
    const IWidget& widget_;

    // expand=True means "I will fight for space"
    // expand=False means "I don't want more space"
    bool expand_;

    // fill=True means "If I got more space, I will occupy it with my content"
    // fill=False means "If I got more space, leave it blank"
    bool fill_;
};

class VBox : public IWidget
{
  public:

    void add(const IWidget& widget, bool expand = true, bool fill = true);

  // IWidget interface implementation

    void render( DisplayArea& display_area ) const override;

    void handleMousePress(const MouseEvent& mouse_event) const override;

    size_t minWidth() const override;

    size_t minHeight() const override;

  private:

    std::list<PackedWidget> packed_widgets_;
};

} // namespace widget

} // namespace colmena

#endif // COLMENA_WIDGETS_BOX_HPP_
