#ifndef COLMENA_WIDGETS_DRAWING_AREA_HPP_
#define COLMENA_WIDGETS_DRAWING_AREA_HPP_

#include "IWidget.hpp"
#include "../DisplayObjectContainer.hpp"

namespace colmena {

namespace widgets {

class DrawingArea : public IWidget, public DisplayObjectContainer
{
  public:

  // IWidget interface implementation

    void render( DisplayArea& display_area ) const override
    { DisplayObjectContainer::render( display_area ); }

    void handleMousePress(const MouseEvent& mouse_event) const override
    { DisplayObjectContainer::handleMousePress( mouse_event ); }

    size_t minWidth() const override
    { return 0; }

    size_t minHeight() const override
    { return 0; }
};

} // namespace widget

} // namespace colmena

#endif // COLMENA_WIDGETS_DRAWING_AREA_HPP_
