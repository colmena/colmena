#ifndef COLMENA_WIDGETS_BUTTON_HPP_
#define COLMENA_WIDGETS_BUTTON_HPP_

#include "IWidget.hpp"
#include "../../Event/Event.hpp"
#include "../resources/Text.hpp"

namespace colmena {

namespace widgets {

class Button : public IWidget
{
  public:

    Button( const std::string& text );

    Event<>& onClick()
    { return on_click_; }

  // IWidget interface implementation

    void render( DisplayArea& display_area ) const override;

    void handleMousePress(const MouseEvent& mouse_event) const override;

    size_t minWidth() const override
    { return label_.getSize().GetX(); }

    size_t minHeight() const override
    { return label_.getSize().GetY(); }

  private:

    Text label_;

    Event<> on_click_;
};

} // namespace widget

} // namespace colmena

#endif // COLMENA_WIDGETS_BUTTON_HPP_
