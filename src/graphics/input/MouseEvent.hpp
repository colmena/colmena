/**
 * File:   MouseEvent.hpp
 * Author: tfischer
 *
 * Colmena, a game engine, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef COLMENA_GRAPHICS_INPUT_MOUSE_EVENT_HPP_
#define COLMENA_GRAPHICS_INPUT_MOUSE_EVENT_HPP_

#include "../../math/Point2D.hpp"
#include "../../Event/Event.hpp"

class MouseEvent
{
  public:

    enum Button {BUTTON_LEFT, BUTTON_MIDDLE, BUTTON_RIGHT};

    MouseEvent(const Position2D& position, const Button& button);

    const Position2D& getPosition() const
    { return position_; }

    const Button& getButton() const
    { return button_; }

  private:

    Position2D position_;

    Button button_;
};

#endif // COLMENA_GRAPHICS_INPUT_MOUSE_EVENT_HPP_
