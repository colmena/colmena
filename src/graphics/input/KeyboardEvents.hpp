#ifndef KEYBOARD_EVENTS_H
#define KEYBOARD_EVENTS_H

#include "../../Event/Event.hpp"
#include "../../math/Direction.hpp"

class KeyboardEvents
{
  public:

    static Event<char>& onCharacterPressed()
    { return on_character_pressed_; }

    static Event<Direction>& onArrowPressed()
    { return on_arrow_pressed_; }

  private:

    static Event<char> on_character_pressed_;
    static Event<Direction> on_arrow_pressed_;
};

#endif
