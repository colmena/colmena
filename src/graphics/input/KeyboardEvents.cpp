#include "KeyboardEvents.hpp"

Event<char> KeyboardEvents::on_character_pressed_;
Event<Direction> KeyboardEvents::on_arrow_pressed_;
