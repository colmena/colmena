/**
 * File:   DisplayArea.hpp
 * Author: tfischer
 *
 * Colmena, a game engine, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef COLMENA_GRAPHICS_DISPLAYAREA_HPP_
#define COLMENA_GRAPHICS_DISPLAYAREA_HPP_

#include <SFML/Graphics.hpp>

#include "../math/Rect.hpp"

// forward declarations
class MainWindow;
class Resource;

/**
 * Represents a portion of the main window canvas,
 * where a resource can be drawn onto.
 */
class DisplayArea
{
  public:

    /**
     * Build a DisplayArea as a sub-area of this one.
     *
     * @param clip
     *   a rectangle representing the subarea to be used.
     *   This rectangle is cropped to fit into its parent (this one).
     */
    DisplayArea getSubArea(const Rect& clip) const;

    /**
     * Draw a Resource into this area. If the resource doesn't fit,
     * it is cropped to fit.
     *
     * @param resource
     *   The resource that will be drawn to the area.
     *
     * @param position
     *   The position where the resource will be placed,
     *
     * @param clip
     *   Cropped region of the resource to draw.
     */
    void draw(const Resource& resource, const Position2D& position, Rect* clip = NULL);

    const Size& getSize() const
    { return area_.GetSize(); }

    // Allow MainWindow class to call the main constructor
    friend MainWindow;

  private:

    // reference to SFML main canvas object to draw onto
    sf::RenderWindow& window_;

    // Portion of the main canvas covered by this display area
    Rect area_;

    /**
     * Build a DisplayArea representing the entire canvas
     */
    DisplayArea(sf::RenderWindow& window);

    /**
     * Constructor that will be used when calling GetSubArea
     */
    DisplayArea(sf::RenderWindow& window, const Rect& area);
};

#endif  /* COLMENA_GRAPHICS_DISPLAYAREA_HPP_ */
