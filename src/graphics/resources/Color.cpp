/* 
 * File:   Color.cpp
 * Author: tfischer
 * 
 * Created on June 21, 2013, 11:46 AM
 */

#include "Color.hpp"

Color::Color(uint8_t r, uint8_t g, uint8_t b, uint8_t a)
  : sfml_color_(r, g, b, a)
{}
