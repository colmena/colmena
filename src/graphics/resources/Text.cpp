#include "Text.hpp"
//~ #include "../../ApplicationLog.hpp"

Text::Text(const sf::Font& font, const std::string& text, uint32_t size, const Color& color, uint32_t style)
	: text_(text, font, size)
{
	// set the color
	text_.setColor( color.sfml_color_ );

	// set the text style
	text_.setStyle( style );

	size_ = Size(getWidth(font, text, size, style & Bold), font.getLineSpacing(size));
}

Text::~Text()
{}

const Size& Text::getSize() const
{
	return size_;
}

void Text::blitTo(sf::RenderWindow& window, const Position2D& position, const Rect* clip) const
{
	sf::Text& text = const_cast<sf::Text&>(text_);

	text.setPosition(sf::Vector2f(position.GetX(), position.GetY()));

	//~ GEL_DEBUG("%s, (%u, %u), (%f, %f)", text.getString().toAnsiString().c_str(), position.GetX(), position.GetY(), text.getOrigin().x, text.getOrigin().y);

	window.draw( text );
}

uint32_t Text::getWidth(const sf::Font& font, const std::string& text, uint32_t size, bool bold)
{
	uint32_t width = 0;

	auto it = text.begin();

	while( it != text.end() )
	{
		char c = *it;

		width += font.getGlyph(c, size, bold).bounds.width;

		it++;
		
		if( it != text.end() ) {
			width += font.getKerning(c, *it, size);
		}
	}
	
	return width;
}
