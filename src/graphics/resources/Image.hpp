/**
 * File:   Image.hpp
 * Author: tfischer
 *
 * Colmena, a game engine, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef COLMENA_GRAPHICS_RESOURCES_IMAGE_HPP_
#define COLMENA_GRAPHICS_RESOURCES_IMAGE_HPP_

#include <SFML/Graphics.hpp>
#include "Resource.hpp"
#include "../../math/Rect.hpp"

/** Representation of a graphical image resource. */
class Image : public Resource
{
  public:

    Image(const std::string& filename);

    Image(const Image& image, const Rect& clip);

    ~Image();

    const Size& getSize() const;

  protected:

    virtual void blitTo(sf::RenderWindow& window, const Position2D& position, const Rect* clip) const override;

  private:

    sf::Texture img_;

    Size size_;
};

#endif  /* COLMENA_GRAPHICS_RESOURCES_IMAGE_HPP_ */
