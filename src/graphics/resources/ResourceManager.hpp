/**
 * Author: tfischer
 *
 * Colmena, a game engine, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef COLMENA_GRAPHICS_RESOURCES_RESOURCE_MANAGER_HPP_
#define COLMENA_GRAPHICS_RESOURCES_RESOURCE_MANAGER_HPP_

#include "Image.hpp"
#include "Font.hpp"

/**
 * Loads resources on-demand and allows their re-utilization by storing
 * them in cache-like container.
 */
class ResourceManager
{
	public:

		/**
		 * Request a resource by filename. If not loaded, generate a new 
		 * resource, else, use the one avaible.
		 */
		static const Image& getImage( const std::string& name );

		/**
		 * TODO
		 */
		static const Text getText( const std::string& text, const std::string& font_name, uint32_t size, const Color& color, uint32_t style = Text::Regular );

    static const Font& getFont( const std::string& font_name );

	private:

		static std::map<std::string, Image> images_;

		static std::map<std::string, Font> fonts_;
};

#endif // COLMENA_GRAPHICS_RESOURCES_RESOURCE_MANAGER_HPP_
