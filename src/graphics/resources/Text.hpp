/**
 * Author: tfischer
 *
 * Colmena, a game engine, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef COLMENA_GRAPHICS_RESOURCES_TEXT_HPP_
#define COLMENA_GRAPHICS_RESOURCES_TEXT_HPP_

#include <string>
#include <SFML/Graphics.hpp>

#include "Color.hpp"
#include "Resource.hpp"
#include "../../math/Rect.hpp"

class Font;

/** Representation of a graphical image resource. */
class Text : public Resource
{
  public:

    const static uint32_t Regular = sf::Text::Regular;
    const static uint32_t Bold = sf::Text::Bold;
    const static uint32_t Italic = sf::Text::Italic;
    const static uint32_t Underlined = sf::Text::Underlined;

    ~Text();

    const Size& getSize() const;

  protected:

    // overloaded from Resource
    virtual void blitTo(sf::RenderWindow& window, const Position2D& position, const Rect* clip) const override;

  private:

    sf::Text text_;

    Size size_;

    friend class Font;

    Text(const sf::Font& font, const std::string& text, uint32_t size, const Color& color, uint32_t style = Text::Regular);

    static uint32_t getWidth(const sf::Font& font, const std::string& text, uint32_t size, bool bold);
};

#endif // COLMENA_GRAPHICS_RESOURCES_TEXT_HPP_
