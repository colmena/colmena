/**
 * File:   ColorRect.cpp
 * Author: tfischer
 *
 * Colmena, a game engine, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "ColorRect.hpp"
#include <assert.h>
#include "../../ApplicationLog.hpp"

ColorRect::ColorRect(Color color)
{
  rectangle_.setFillColor(color.sfml_color_);
}

void ColorRect::blitTo(sf::RenderWindow& window, const Position2D& position, const Rect* clip) const
{
  // @author tfischer: since this represents an infinite color plane, 
  // the draw area should be defined by the caller
  assert(clip!=NULL);

  sf::RectangleShape& rectangle = const_cast<sf::RectangleShape&>(rectangle_);
  rectangle.setPosition(position.GetX(), position.GetY());

  // set size
  sf::Vector2f size(clip->GetWidth(), clip->GetHeight());
  rectangle.setSize(size);
  
  window.draw(rectangle);
}
