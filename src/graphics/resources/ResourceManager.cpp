#include "ResourceManager.hpp"

const Image& ResourceManager::getImage( const std::string& name )
{
	auto it = images_.find( name );
	
	// if resource was loaded before
	if ( it != images_.end() )
		return it->second;

	// if resource was not loaded, load it
	Image img( name );

	std::pair<std::map<std::string, Image>::iterator, bool>  bla = images_.insert( std::pair<std::string, Image>(name, img) );
	
	return (bla.first)->second;
}

const Text ResourceManager::getText(const std::string& text, const std::string& font_name, uint32_t size, const Color& color, uint32_t style)
{
	const Font& font = getFont( font_name );
	
	return font.getText(text, size, color, style);
}

const Font& ResourceManager::getFont( const std::string& font_name )
{
  auto it = fonts_.find( font_name );
	
	// if font was loaded before
	if ( it != fonts_.end() )
		return it->second;

	// if resource was not loaded, load it
	Font font( font_name );

	std::pair<std::map<std::string, Font>::iterator, bool>  bla = fonts_.insert( std::pair<std::string, Font>(font_name, font) );

  return (bla.first)->second;
}

std::map<std::string, Image> ResourceManager::images_;

std::map<std::string, Font> ResourceManager::fonts_;
