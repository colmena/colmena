/**
 * File:   Image.cpp
 * Author: tfischer
 *
 * Colmena, a game engine, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "Image.hpp"
#include "../../ApplicationLog.hpp"

Image::Image(const std::string& filename)
{
  if ( !img_.loadFromFile(filename) )
    GEL_ERROR("'%s' could not be opened", filename.c_str());

  sf::Vector2u size = img_.getSize();
  size_ = Size(size.x, size.y);
}

Image::Image(const Image& image, const Rect& clip)
{
  sf::IntRect sf_clip(clip.GetX(), clip.GetY(), clip.GetWidth(), clip.GetHeight());
  if ( !img_.loadFromImage(image.img_.copyToImage(), sf_clip) )
    GEL_ERROR("Image could not be copied");

  size_ = clip.GetSize();
}

Image::~Image()
{}

const Size& Image::getSize() const
{
  return size_;
}

void Image::blitTo(sf::RenderWindow& window, const Position2D& position, const Rect* clip) const
{
  sf::Sprite sprite;
  
  if ( clip == NULL )
  {
    sprite = sf::Sprite(img_);
  }
  else
  {
    sf::IntRect sf_rect(clip->GetX(), clip->GetY(), clip->GetWidth(), clip->GetHeight());
    sprite = sf::Sprite(img_, sf_rect);
  }

  sprite.setPosition(sf::Vector2f(position.GetX(), position.GetY()));

  window.draw(sprite);
}
