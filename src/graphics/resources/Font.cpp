#include "Font.hpp"
#include "../../ApplicationLog.hpp"

Font::Font(const std::string& font_name)
{
	if ( not font_.loadFromFile( font_name ) ) {
		GEL_ERROR("Error loading font %s", font_name.c_str());
	}
}

Text Font::getText(const std::string& text, uint32_t size, const Color& color, uint32_t style) const
{
	return Text( font_, text, size, color, style );
}

Size Font::getCharSize(uint32_t char1, uint32_t size, bool bold) const
{
	return font_.getGlyph(char1, size, bold).bounds.width;
}

uint32_t Font::getKerning(uint32_t char1, uint32_t char2, uint32_t size) const
{
	return font_.getKerning(char1, char2, size);
}

uint32_t Font::getLineHeight(uint32_t size) const
{
	return font_.getLineSpacing(size);
}
