/**
 * Author: tfischer
 *
 * Colmena, a game engine, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef COLMENA_GRAPHICS_RESOURCES_FONT_HPP_
#define COLMENA_GRAPHICS_RESOURCES_FONT_HPP_

#include <string>
#include <SFML/Graphics/Font.hpp>

#include "Text.hpp"
#include "Color.hpp"

class Font
{
	public:

		Font(const std::string& font_name);

		Text getText(const std::string& text, uint32_t size, const Color& color, uint32_t style = Text::Regular) const;

		Size getCharSize(uint32_t char1, uint32_t size, bool bold) const;

		uint32_t getKerning(uint32_t char1, uint32_t char2, uint32_t size) const;

		uint32_t getLineHeight(uint32_t size) const;

	private:

		sf::Font font_;
		
};

#endif // COLMENA_GRAPHICS_RESOURCES_FONT_HPP_
