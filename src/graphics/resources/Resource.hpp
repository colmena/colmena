/**
 * File:   Resource.hpp
 * Author: tfischer
 *
 * Colmena, a game engine, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef COLMENA_GRAPHICS_RESOURCES_RESOURCE_HPP_
#define COLMENA_GRAPHICS_RESOURCES_RESOURCE_HPP_

#include <SFML/Graphics.hpp>
#include "../../math/Rect.hpp"
#include "../../math/Point2D.hpp"

/** forward declaration of screen to establish friend relationship */
class DisplayArea;

/** Representation of a graphical image resource. */
class Resource
{
  public:

    virtual ~Resource() {}

  protected:

    /** Only to be used by Screen::Draw(Resource...) */
    virtual void blitTo(sf::RenderWindow& window, const Position2D& position, const Rect* clip) const = 0;

  private:

    friend class DisplayArea;
};

#endif  /* COLMENA_GRAPHICS_RESOURCES_RESOURCE_HPP_ */

