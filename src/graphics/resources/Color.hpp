/* 
 * File:   Color.hpp
 * Author: tfischer
 *
 * Created on June 21, 2013, 11:46 AM
 */

#ifndef COLOR_HPP
#define	COLOR_HPP

#include <stdint.h>
#include <SFML/Graphics/Color.hpp>

// forward declaration for friend
class ColorRect;
class Text;

class Color
{
  public:
    
    Color(uint8_t r, uint8_t g, uint8_t b, uint8_t a = 255);
    
  private:
    
    sf::Color sfml_color_;
    
    friend ColorRect;
    friend Text;
};

#endif	/* COLOR_HPP */

