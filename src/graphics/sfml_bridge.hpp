#include <SFML/System.hpp>
#include "../math/Point2D.hpp"

inline Point2w fromSFML(const sf::Vector2u& pt)
{ return Point2w(pt.x, pt.y); }
