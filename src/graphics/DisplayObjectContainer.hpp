/**
 * File:   DisplayObjectContainer.hpp
 * Author: tfischer
 *
 * Colmena, a game engine, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef COLMENA_GRAPHICS_DISPLAYOBJECTCONTAINER_HPP_
#define COLMENA_GRAPHICS_DISPLAYOBJECTCONTAINER_HPP_

#include <set>

#include "DisplayObject.hpp"
#include "../useful_macros.hpp"

class DisplayObjectContainer : public DisplayObject
{
    public:

        /**
         * Adds a drawable object to the given layer,
         * where it will be rendered by this container.
         *
         * @param object
         *   view to be added
         *
         * @return
         *   false if the object already belong to the container.
         *   true otherwise.
         */
        bool add(DisplayObject* object);

        /**
         * Removes a drawable object from the container.
         *
         * @param object
         *   view to be removed
         *
         * @return
         *   false if the object already belong to the container.
         *   true otherwise.
         */
        bool remove(DisplayObject* object);

        /**
         * This function is called to render a frame of the game screen
         * to a given surface.
         *
         * @param display_area
         *   surface on which to render.
         */
        virtual void render(DisplayArea& display_area) const override;

        /**
         * Pass mouse event down to children
         */
        virtual void handleMousePress(const MouseEvent& mouse_event) const override;

    private:

        /* the lists of objects, one for each layer */
        std::set<DisplayObject*> objects_;

};

#endif // COLMENA_GRAPHICS_DISPLAYOBJECTCONTAINER_HPP_
