/**
 * File:   DisplayObject.hpp
 * Author: tfischer
 *
 * Colmena, a game engine, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef COLMENA_GRAPHICS_DISPLAY_OBECT_HPP_
#define COLMENA_GRAPHICS_DISPLAY_OBECT_HPP_

#include "DisplayArea.hpp"
#include "input/MouseEvent.hpp"

/* Interface for drawable objects that can be drawn to a surface */
class DisplayObject
{
  public:

    virtual ~DisplayObject() {}

    /**
     * Renders the object to a surface.
     *
     * @param display_area
     *   the surface where to draw on
     */
    virtual void render(DisplayArea& display_area) const = 0;

    /**
     * Interface to handle mouse events.
     */
    virtual void handleMousePress(const MouseEvent& mouse_event) const {}
};

#endif // COLMENA_GRAPHICS_DISPLAY_OBECT_HPP_
