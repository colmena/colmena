/**
 * File:   LayeredDisplayObjectContainer.hpp
 * Author: tfischer
 *
 * Colmena, a game engine, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef COLMENA_GRAPHICS_DISPLAYOBECTS_LAYEREDDISPLAYOBJECTCONTAINER_HPP_
#define COLMENA_GRAPHICS_DISPLAYOBECTS_LAYEREDDISPLAYOBJECTCONTAINER_HPP_

#include <vector>
#include "../DisplayObject.hpp"
#include "../DisplayObjectContainer.hpp"

class LayeredDisplayObjectContainer : public DisplayObject
{
  public:

    /**
     * Construct a custom sized layered container
     */
    LayeredDisplayObjectContainer(size_t layers);

    /**
     * Adds a display object to the given layer
     * returns true on succes, false otherwise.
     */
    bool add(DisplayObject* object, size_t layer);

    /**
     * Removes a display object from the given layer
     * returns true on succes, false otherwise.
     */
    bool remove(DisplayObject* object, size_t layer);

    /**
     * Renders the layers in ascending order.
     */
    virtual void render(DisplayArea& display_area) const override;

  private:

    size_t num_layers_;

    std::vector<DisplayObjectContainer> layers_;
};

#endif  /* COLMENA_GRAPHICS_DISPLAYOBECTS_LAYEREDDISPLAYOBJECTCONTAINER_HPP_ */
