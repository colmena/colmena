/**
 * File:   Spritesheet.hpp
 * Author: tfischer
 *
 * Colmena, a game engine, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef COLMENA_GRAPHICS_DISPLAYOBECTS_SPRITESHEET_HPP_
#define COLMENA_GRAPHICS_DISPLAYOBECTS_SPRITESHEET_HPP_

#include "Sprite.hpp"
#include "../resources/Image.hpp"

class Spritesheet : public Sprite
{
  public:

    Spritesheet(const Image& resource, const Size& grid_size, const Position2D& position = Position2D::Null());

    virtual void render(DisplayArea& display_area) const override;

    void select(uint32_t n);

    void selectRow(uint32_t row);

    void selectCol(uint32_t col);

  private:

    /** active sprite index on the grid */
    Position2D current_pos_;

    /** number of rows and columns of the matrix */
    Size grid_size_;

    /** size of a single sprite (all should have the same size) */
    Size sprite_size_;
};

#endif // COLMENA_GRAPHICS_DISPLAYOBECTS_SPRITESHEET_HPP_
