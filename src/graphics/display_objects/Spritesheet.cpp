/**
 * File:   Spritesheet.cpp
 * Author: tfischer
 *
 * Colmena, a game engine, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "Spritesheet.hpp"
#include "../../ApplicationLog.hpp"

Spritesheet::Spritesheet(const Image& resource, const Size& grid_size, const Position2D& position)
  : Sprite(resource, position), current_pos_(Position2D::Null())
  , grid_size_(grid_size), sprite_size_(resource.getSize()/grid_size_)
{}

void Spritesheet::render(DisplayArea& display_area) const
{
  Size clip_position = current_pos_ * sprite_size_;
  Rect clip(clip_position, sprite_size_);

  display_area.draw(resource_, position_, &clip);
}

void Spritesheet::select(uint32_t n)
{
  uint32_t row = n / grid_size_.GetX();
  uint32_t col = n % grid_size_.GetX();

  current_pos_ = Size(col, row);
}

void Spritesheet::selectRow(uint32_t row)
{
  uint32_t col = current_pos_.GetX();
  current_pos_ = Position2D(col, row);
}

void Spritesheet::selectCol(uint32_t col)
{
  uint32_t row = current_pos_.GetY();
  current_pos_ = Position2D(col, row);
}
