/**
 * File:   LayeredDisplayObjectContainer.cpp
 * Author: tfischer
 *
 * Colmena, a game engine, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "LayeredDisplayObjectContainer.hpp"
#include "../../useful_macros.hpp"

LayeredDisplayObjectContainer::LayeredDisplayObjectContainer(size_t layers)
  : num_layers_(layers), layers_(layers)
{}

bool LayeredDisplayObjectContainer::add(DisplayObject* object, size_t layer)
{
  // TODO throw exception
  if ( num_layers_ <= layer )
    return false;

  return layers_[layer].add(object);
}

bool LayeredDisplayObjectContainer::remove(DisplayObject* object, size_t layer)
{
  // TODO throw exception
  if ( num_layers_ <= layer )
    return false;

  return layers_[layer].remove(object);
}

void LayeredDisplayObjectContainer::render(DisplayArea& display_area) const
{
  forn(i, num_layers_)
    layers_[i].render(display_area);
}
