/**
 * File:   MainWindow.cpp
 * Author: tfischer
 *
 * Colmena, a game engine, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "MainWindow.hpp"
#include "input/MouseEvent.hpp"
#include "input/KeyboardEvents.hpp"
#include "../ApplicationLog.hpp"

MainWindow::MainWindow(const std::string& title, const Size& size)
  :  window_(sf::VideoMode(size.GetX(), size.GetY(), 32), title)
  , display_area_( window_ )
{}

void MainWindow::mainLoop()
{
  bool quit = false;
  sf::Event event;

  //While the user hasn't quit
  while( !quit )
  {
    //While there's an event to handle
    while ( window_.pollEvent(event) )
    {
      // quit application
      if (event.type == sf::Event::Closed)
        quit=true;

      //If a key was pressed
      if (event.type == sf::Event::KeyPressed)
      {
        switch( event.key.code )
        {
          case sf::Keyboard::Up:
            KeyboardEvents::onArrowPressed().Notify( Direction::N );
            break;
          case sf::Keyboard::Down:
            KeyboardEvents::onArrowPressed().Notify( Direction::S );
            break;
          case sf::Keyboard::Left:
            KeyboardEvents::onArrowPressed().Notify( Direction::W );
            break;
          case sf::Keyboard::Right:
            KeyboardEvents::onArrowPressed().Notify( Direction::E );
            break;
          default:
            break;
        }
      }

      // If the mouse moved
      if (event.type == sf::Event::MouseButtonPressed)
      {
        Position2D position(event.mouseButton.x, event.mouseButton.y);
        MouseEvent::Button button;

        switch( event.mouseButton.button )
        {
          case sf::Mouse::Left:
            button = MouseEvent::BUTTON_LEFT;
            break;
          case sf::Mouse::Middle:
            button = MouseEvent::BUTTON_MIDDLE;
            break;
          case sf::Mouse::Right:
            button = MouseEvent::BUTTON_RIGHT;
            break;
          default:
            break;
        }

        handleMousePress( MouseEvent(position, button) );
      }
    }

    render();
  }
}

void MainWindow::render()
{
  // Clear screen
  window_.clear();

  // Draw everything attached to the window
  DisplayObjectContainer::render( display_area_ );

  // Update the window
  window_.display();
}
