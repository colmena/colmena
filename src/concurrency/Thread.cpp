/**
 * File:   Thread.cpp
 * Author: tfischer
 *
 * Colmena, a game engine, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <signal.h>
#include <pthread.h>

#include "Thread.hpp"
#include "../ApplicationLog.hpp"

Thread::IFunctionWrapper::~IFunctionWrapper()
{};

Thread::~Thread()
{
  delete _function_wrapper;
}

bool Thread::Run()
{
  /* Initialize and set thread joinable attribute */
  pthread_attr_t attr;
  pthread_attr_init(&attr);
  pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

  // pthread_create returns 0 on success
  int ret = pthread_create(&_thread, &attr, &Thread::Wrapper, this);
  GEL_DEBUG("Created thread %lu", _thread);
  /* Free attribute */
  pthread_attr_destroy(&attr);

  if ( ret==0 )
  {
    _thread_created = true;
    return true;
  }

  if ( ret == EAGAIN )
    GEL_ERROR("Insufficient resources to create another thread, or a system-imposed limit on the number of threads was encountered");
  else if ( ret == EINVAL )
    GEL_ERROR("Invalid settings in attr");
  else if ( ret == EPERM )
    GEL_ERROR("No permission to set the scheduling policy and parameters specified in attr");

  return false;
}

bool Thread::Join()
{
  GEL_DEBUG("Joining thread %lu", _thread);
  int ret = _thread_created ? pthread_join(_thread, NULL) : 0;

  if ( ret == 0 )
    return true;

  GEL_ERROR("Joining thread %lu: %s", _thread, ERRNO_STR);

  return false;
}

bool Thread::Kill()
{
  GEL_DEBUG("Killing thread %lu", _thread);
  int ret = _thread_created ? pthread_kill(_thread, SIGKILL) : 0;

  if ( ret==0 )
    return true;

  GEL_ERROR("Killing thread %lu: %s", _thread, ERRNO_STR);

  return false;
}

void* Thread::Wrapper(void* arg)
{
    Thread* instance_ptr = (Thread*)arg;
    (*(instance_ptr->_function_wrapper))();
    
    // return anything to comply with the functor standard arity
    return NULL;
}
