#ifndef CONCURRENT_QUEUE_HPP_
#define CONCURRENT_QUEUE_HPP_

#include <queue>
#include <mutex>
#include <condition_variable>

/**
 * @brief Built for SINGLE consumer and SINGLE publisher!!!
 * based on
 * - https://www.justsoftwaresolutions.co.uk/threading/implementing-a-thread-safe-queue-using-condition-variables.html
 * - http://www.internetmosquito.com/2011/04/making-thread-safe-queue-in-c-i.html
 */
template <typename T>
class ConcurrentQueue
{
  public:

    ConcurrentQueue();

    bool empty() const
    {
      std::unique_lock<std::mutex> lock( queue_access_mutex_ );
      return queue_.empty();
    }

    size_t size() const
    {
      std::unique_lock<std::mutex> lock( queue_access_mutex_ );
      return queue_.size();
    }

    T& front()
    {
      std::unique_lock<std::mutex> lock( queue_access_mutex_ );
      return 
    }

    const T& front() const
    {
      std::unique_lock<std::mutex> lock( queue_access_mutex_ );
      return queue_.front();
    }

    void push( const T& val );

    // extra functions

    /**
     * @brief
     *   Block while the queue is empty. As soon as the queue
     *   is filled, return the front element and pop it.
     *   This is equivalent to calling wait() front() and pop()
     *   sequentially, but much faster since the mutex lock
     *   is performed only once.
     * 
     * @param val
     *   Reference where the popped value will be stored.
     *   If the queue was stopped while waiting on this function,
     *   the referenced value should be considered invalid.
     * 
     * @return
     *   On success, the function returns a positive integer if there
     *   are more elements that remain queued, or 0 if the queue is
     *   empty after the pop.
     *   If the queue was stopped while waiting on this function,
     *   the returned value is -1.
     */
    int waitAndPop(T& val);

    /**
     * @brief Block while the queue is loaded. This may be useful when
     *   waiting for the consumer thread to finish before shutting down
     *   the program.
     */
    void waitUntilEmpty() const;

    /**
     * Raises a flag to stop the queue and signals all condition
     * variables to wake up. If a consumer is waiting on a pop, the
     * popped value should be considererd invalid.
     */
    void stop();

  private:

    std::queue<T> queue_;

    std::atomic_bool stop_;

    mutable std::mutex queue_access_mutex_;

    mutable std::condition_variable is_empty_;

    mutable std::condition_variable not_empty_;
};

template <typename T>
ConcurrentQueue<T>::ConcurrentQueue()
  : stop_( false )
{}

template <typename T>
void ConcurrentQueue<T>::push( const T& val )
{
  bool was_empty;

  {
    std::unique_lock<std::mutex> lock( queue_access_mutex_ );
    was_empty = queue_.empty();
    queue_.push( val );
  }

  // TODO que pasa si justo acá se elimina el elemento (pop)? Es cierto
  // que la cola dejó de estar vacía, pero para cuando se ejecute el
  // thread que estaba esperando un elemento, ya podría haber un elemento
  // nuevamente.
  // Para que esto suceda, tiene que haber un thread que haga pop(),
  // mientras otro esta en waitUntilData(), mientras otro está ejecutando
  // este push, es decir, al menos 2 consumidores. Si tenemos un único
  // consumidor, esto anda.

  if ( was_empty )
    not_empty_.notify_one();
}

template <typename T>
bool ConcurrentQueue<T>::pop(T& val)
{
  bool const is_empty;

  {
    std::unique_lock<std::mutex> lock( queue_access_mutex_ );
    val = queue_.front();
    queue_.pop();
    is_empty = queue_.empty();
  }

  // TODO que pasa si justo acá se inserta un elemento? Es cierto
  // que la cola se vació, pero para cuando se ejecute el thread
  // que estaba esperando que se vacíe, ya podría haber un elemento
  // nuevamente.
  // Para que esto suceda, tiene que haber un thread que haga push(),
  // mientras otro esta en waitUntilEmpty(), mientras otro está ejecutando
  // este pop, es decir, al menos 3 threads. Si tenemos únicamente 2,
  // esto anda.

  if ( is_empty )
    is_empty_.notify_one();
}

template <typename T>
int ConcurrentQueue<T>::waitAndPop(T& val)
{
  bool is_empty = false;

  {
    std::unique_lock<std::mutex> lock( queue_access_mutex_ );
    
    while ( not stop_ and queue_.empty() )
      not_empty_.wait( lock );

    if ( stop_ )
      return -1;

    val = queue_.front();

    queue_.pop();
    is_empty = queue_.empty();
  }

  if ( not is_empty )
    return 1;

  // if ( is_empty and not stop ) :

  is_empty_.notify_one();

  return 0;
}

template <typename T>
void ConcurrentQueue<T>::waitUntilEmpty() const
{
  std::unique_lock<std::mutex> lock( queue_access_mutex_ );
  
  while ( not stop_ and not queue_.empty() )
    is_empty_.wait( lock );
}

template <typename T>
void ConcurrentQueue<T>::stop()
{
  stop_ = true;
  is_empty_.notify_all();
  not_empty_.notify_all();
}

#endif
