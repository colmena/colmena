/**
 * File:   ThreadedLoop.hpp
 * Author: tfischer
 * 
 * Colmena, a game engine, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef COLMENA_CONCURRENCY_THREADED_LOOP_HPP_
#define COLMENA_CONCURRENCY_THREADED_LOOP_HPP_

#include <atomic>

#include "Thread.hpp"

class ThreadedLoop : public Thread
{
  public:

    ThreadedLoop();

    virtual ~ThreadedLoop();

    /** Starts the loop on a separate thread */
    bool StartLoop();

    /** Non-blocking. Doesnt wait for the loop to stop,
     * just triggers the flag that will exit the loop */
    void StopLoop();

  protected:

    virtual bool LoopHandler( void ) = 0;

  private:

    /** boolean that handles main loop quitting */
    std::atomic<bool> stop_;

    void Loop();
};

#endif  /* COLMENA_CONCURRENCY_THREADED_LOOP_HPP_ */
