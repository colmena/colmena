/**
 * File:   ThreadSafeQueue.hpp
 * Author: tfischer
 *
 * Colmena, a game engine, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef COLMENA_CONCURRENCY_THREADED_SAFE_QUEUE_HPP_
#define COLMENA_CONCURRENCY_THREADED_SAFE_QUEUE_HPP_

#include <queue>
#include <mutex>

template <class T>
class ThreadSafeQueue
{
    public:

        void push(const T& elem);

        T& pop();

        bool empty() const;

    private:

        std::queue<T> queue_;

        std::mutex queue_mutex_;

};

template <class T>
void ThreadSafeQueue<T>::push(const T& elem)
{
  std::lock_guard<std::mutex> lock_( queue_mutex_ );

  queue_.push(elem);
}

template <class T>
T& ThreadSafeQueue<T>::pop()
{
  std::lock_guard<std::mutex> lock_( queue_mutex_ );

  T& elem =queue_.front();
  queue_.pop();

  return elem;
}

template <class T>
bool ThreadSafeQueue<T>::empty() const
{
  //~ std::lock_guard<std::mutex> lock_( queue_mutex_ );

  return queue_.empty();
}

#endif  /* COLMENA_CONCURRENCY_THREADED_SAFE_QUEUE_HPP_ */
