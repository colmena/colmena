/**
 * File:   ThreadedLoop.cpp
 * Author: tfischer
 * 
 * Colmena, a game engine, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "ThreadedLoop.hpp"
#include "../ApplicationLog.hpp"

ThreadedLoop::ThreadedLoop()
  : Thread(this, &ThreadedLoop::Loop), stop_(false)
{}

ThreadedLoop::~ThreadedLoop()
{
  StopLoop();
}

void ThreadedLoop::StopLoop()
{
  stop_ = true;
}

bool ThreadedLoop::StartLoop()
{
  return this->Run();
}

void ThreadedLoop::Loop()
{
  GEL_DEBUG("Entering main loop");

  bool quit = false;
  while(!quit)
  {
    bool res = LoopHandler();

    quit = !res || stop_;
  }

  GEL_DEBUG("Leaving main loop");
}
