/**
 * File:   Thread.hpp
 * Author: tfischer
 * 
 * Colmena, a game engine, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef COLMENA_CONCURRENCY_THREAD_HPP_
#define COLMENA_CONCURRENCY_THREAD_HPP_

#include <thread>

class Thread
{
  private:

    class IFunctionWrapper
    {
      public:
        virtual ~IFunctionWrapper();
        virtual void operator ()() = 0;
    };

    template <typename P>
    class ArgFunctionWrapper : public IFunctionWrapper
    {
      private:
        typedef void (*function_ptr_t)(P);
      public:
        ArgFunctionWrapper(function_ptr_t function_ptr, P argument);
        void operator ()();
      private:
        function_ptr_t _function_ptr;
        P _argument;
    };

    template <typename C>
    class MemberFunctionWrapper : public IFunctionWrapper
    {
      private:
        typedef void (C::*member_function_ptr_t)();
      public:
        MemberFunctionWrapper(C* object_ptr, member_function_ptr_t member_function_ptr);
        virtual void operator ()();
      private:
        C* _object_ptr;
        member_function_ptr_t _member_function_ptr;
    };

    template <typename C, typename P>
    class MemberWithArgFunctionWrapper : public IFunctionWrapper
    {
      private:
        typedef void (C::*member_function_ptr_t)(P);
      public:
        MemberWithArgFunctionWrapper(C* object_ptr, member_function_ptr_t member_function_ptr, P param);
        void operator ()();
      private:
        P _param;
        C* _object_ptr;
        member_function_ptr_t _member_function_ptr;
    };

  public:

    /**
     * @brief Constructor for a thread calling a one argument function
     * @param function
     * @param argument
     */
    template <typename P>
    Thread(void (*function_ptr)(P), P argument);

    /**
     * @brief Constructor for a thread calling a member function with no parameters.
     * @param object_ptr a pointer to the object.
     * @param member_function_ptr a pointer to the member function.
     */
    template <typename C>
    Thread(C* object_ptr, void (C::*member_function_ptr)());

    /**
     * @brief Constructor for a thread calling a member function with no parameters.
     * @param object_ptr a pointer to the object.
     * @param member_function_ptr a pointer to the member function.
     */
    template <typename C, typename P>
    Thread(C* object_ptr, void (C::*member_function_ptr)(P), P argument);

    /** Deleting this object does not kill a working thread, which if running will be left as zombie */
    virtual ~Thread();

    bool Run();

    bool Join();

    bool Kill();

  private:

    IFunctionWrapper* _function_wrapper;

    pthread_t _thread;
    bool _thread_created;

    static void* Wrapper(void* arg);
};

// -----------------------------------------------------------------------------

template <typename P>
Thread::ArgFunctionWrapper<P>::ArgFunctionWrapper(function_ptr_t function_ptr, P argument)
  : _function_ptr(function_ptr), _argument(argument)
{}

template <typename P>
void Thread::ArgFunctionWrapper<P>::operator ()()
{
  _function_ptr(_argument);
}

// -----------------------------------------------------------------------------

template <typename C>
Thread::MemberFunctionWrapper<C>::MemberFunctionWrapper(C* object_ptr, member_function_ptr_t member_function_ptr)
  : _object_ptr(object_ptr), _member_function_ptr(member_function_ptr)
{}

template <typename C>
void Thread::MemberFunctionWrapper<C>::operator ()()
{
  (_object_ptr->*_member_function_ptr)();
}

// -----------------------------------------------------------------------------

template <typename C, typename P>
Thread::MemberWithArgFunctionWrapper<C, P>::MemberWithArgFunctionWrapper(C* object_ptr, member_function_ptr_t member_function_ptr, P param)
  : _object_ptr(object_ptr), _member_function_ptr(member_function_ptr), _param(param)
{}

template <typename C, typename P>
void Thread::MemberWithArgFunctionWrapper<C, P>::operator ()()
{
  (_object_ptr->*_member_function_ptr)(_param);
}

// -----------------------------------------------------------------------------

template <typename P>
Thread::Thread(void (*function_ptr)(P), P argument)
  : _function_wrapper( new ArgFunctionWrapper<P>(function_ptr, argument) ), _thread_created(false)
{}

template <typename C>
Thread::Thread(C* object_ptr, void (C::*member_function_ptr)())
  : _function_wrapper( new MemberFunctionWrapper<C>(object_ptr, member_function_ptr) ), _thread_created(false)
{}

template <typename C, typename P>
Thread::Thread(C* object_ptr, void (C::*member_function_ptr)(P), P argument)
  : _function_wrapper( new MemberWithArgFunctionWrapper<C, P>(object_ptr, member_function_ptr, argument) ), _thread_created(false)
{}

#endif /* COLMENA_CONCURRENCY_THREAD_HPP_ */
