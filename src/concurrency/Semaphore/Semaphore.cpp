/**
 * File:   Semaphore.cpp
 * Author: tfischer
 *
 * Colmena, a game engine, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "Semaphore.hpp"
#include "../../ApplicationLog.hpp"

Semaphore::Semaphore()
{
    int ret = sem_init(&_semaphore, 0, 0);

    if ( ret < 0 )
      GEL_FATAL("Initializing semaphore: %s", ERRNO_STR);
}

Semaphore::~Semaphore()
{
    int ret = sem_destroy(&_semaphore);

    if ( ret < 0 )
      GEL_FATAL("Destroying semaphore: %s", ERRNO_STR);
}

bool Semaphore::Wait()
{
    int ret = sem_wait( &_semaphore );

    if ( ret == 0 )
        return true;

    GEL_FATAL("Semaphore could not wait: %s", ERRNO_STR);

    return false;
}

bool Semaphore::Signal()
{
    int ret = sem_post(&_semaphore);

    if ( ret == 0 )
        return true;

    GEL_FATAL("Semaphore could not signal: %s", ERRNO_STR);

    return false;
}
