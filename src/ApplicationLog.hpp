/**
 * File:   ApplicationLog.hpp
 * Author: tfischer
 * 
 * Colmena, a game engine, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef COLMENA_APPLICATION_LOG_HPP_
#define COLMENA_APPLICATION_LOG_HPP_

#include <stdio.h>
#include <errno.h>
#include <string.h>

#define RESET_COLOR "\e[m"
#define MAKE_BLACK "\e[30m"
#define MAKE_RED "\e[31m"
#define MAKE_GREEN "\e[32m"
#define MAKE_YELLOW "\e[33m"
#define MAKE_BLUE "\e[34m"
#define MAKE_MAGENTA "\e[35m"
#define MAKE_CYAN "\e[36m"
#define MAKE_WHITE "\e[37m"
/*
#define SEVERITY_DEBUG 0
#define SEVERITY_INFO 1
#define SEVERITY_WARNING 2
#define SEVERITY_ERROR 3

#define SEVERITY_LEVEL SEVERITY_WARNING
*/
#define GEL_DEBUG(s, ...) do { /*if (SEVERITY_LEVEL <= SEVERITY_DEBUG)*/ printf("[  DEBUG] %s:%s():%d: " s "\n", __FILE__, __func__, __LINE__, ## __VA_ARGS__); } while(0)
#define GEL_INFO(s, ...) do {  /*if (SEVERITY_LEVEL <= SEVERITY_INFO)*/ printf(MAKE_YELLOW "[   INFO] %s:%s():%d: " s RESET_COLOR "\n", __FILE__, __func__, __LINE__, ## __VA_ARGS__); } while(0)
#define GEL_WARNING(s, ...) do {  /*if (SEVERITY_LEVEL <= SEVERITY_WARNING)*/ printf(MAKE_YELLOW "[WARNING] %s:%s():%d: " s RESET_COLOR "\n", __FILE__, __func__, __LINE__, ## __VA_ARGS__); } while(0)
#define GEL_ERROR(s, ...) do {  printf(MAKE_RED "[  ERROR] %s:%s():%d: " s RESET_COLOR "\n", __FILE__, __func__, __LINE__, ## __VA_ARGS__); } while(0)
#define GEL_FATAL(s, ...) do {  printf(MAKE_RED "[  FATAL] %s:%s():%d: " s RESET_COLOR "\n", __FILE__, __func__, __LINE__, ## __VA_ARGS__); } while(0)

#define ERRNO_STR strerror(errno)

#endif /* COLMENA_APPLICATION_LOG_HPP_ */
