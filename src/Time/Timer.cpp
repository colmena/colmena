/**
 * File:   Timer.cpp
 * Author: tfischer
 *
 * Colmena, a game engine, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "Timer.hpp"
#include "../ApplicationLog.hpp"
#include "../useful_macros.hpp"

Timer::Timer()
{
  Reset();
}

void Timer::Reset()
{
  Timer::Now(&_initial_time);
}

uint32_t Timer::Get() const
{
  timespec current_time;
  Timer::Now(&current_time);

  time_t delta_seconds = current_time.tv_sec - _initial_time.tv_sec;
  long delta_nanoseconds = current_time.tv_nsec - _initial_time.tv_nsec;

  uint32_t delta_miliseconds = delta_seconds*1000 + delta_nanoseconds/1000000;

  return delta_miliseconds;
}

bool Timer::Now(timespec* time)
{
  int ret = clock_gettime(CLOCK_MONOTONIC, time);

  if (ret == 0)
    return true;

  GEL_ERROR("Retrieving time of clock: %s", ERRNO_STR);

  return false;
}
