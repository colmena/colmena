/**
 * File:   StopWatch.cpp
 * Author: tfischer
 * 
 * Colmena, a game engine, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "StopWatch.hpp"

StopWatch::StopWatch()
  : _running(false)
{}

void StopWatch::Start(uint32_t time)
{
  _time = time;
  Timer::Reset();
  _running=true;
}

void StopWatch::Stop()
{
  _time=0;
  _running=false;
}

uint32_t StopWatch::GetElapsed()
{
  if (!_running)
    return 0;

  uint32_t elapsed = Timer::Get();

  if ( elapsed < _time )
    return  elapsed;

  Stop();

  return 0;
}

uint32_t StopWatch::GetRemaining()
{
  if (!_running)
    return 0;

  uint32_t elapsed = Timer::Get();

  if ( elapsed < _time )
    return  _time-elapsed;

  Stop();

  return 0;
}

bool StopWatch::IsRunning()
{
  if ( Timer::Get() > _time )
    Stop();

  return _running;
}
