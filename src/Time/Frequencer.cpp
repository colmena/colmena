/**
 * File:   Frequencer.cpp
 * Author: tfischer
 * 
 * Colmena, a game engine, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include <unistd.h>

#include "Frequencer.hpp"
#include "Timer.hpp"
#include "../ApplicationLog.hpp"

Frequencer::Frequencer(uint32_t rate)
  : _frame_time(1000/rate)
{
  Timer::Reset();
}

void Frequencer::Wait()
{
  uint32_t elapsed = Timer::Get();

  if ( elapsed > _frame_time )
    GEL_WARNING("Could not maintain specified rate, frame lasted %d ms", elapsed);
  else
    usleep( 1000*(_frame_time-elapsed) );

  Timer::Reset();
}
