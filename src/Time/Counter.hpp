/**
 * File:   Counter.hpp
 * Author: tfischer
 * 
 * Colmena, a game engine, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef COLMENA_TIME_COUNTER_HPP_
#define COLMENA_TIME_COUNTER_HPP_

#include <time.h>
#include <stdint.h>

#include "Timer.hpp"

/**
 * Circularily counts up to a limit at a given speed (given in miliseconds)
 */
class Counter : public Timer
{
  public:

    /**
     * @param speed
     *   speed of a tick in miliseconds
     */
    Counter(uint32_t speed);

    /**
     * @return
     *   current tick value
     */
    uint32_t Get() const;

  private:

    uint32_t _speed;
};

#endif /* COLMENA_TIME_COUNTER_HPP_ */
