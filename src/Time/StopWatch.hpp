/**
 * File:   StopWatch.hpp
 * Author: tfischer
 * 
 * Colmena, a game engine, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef COLMENA_TIME_STOPWATCH_HPP_
#define COLMENA_TIME_STOPWATCH_HPP_

#include "Timer.hpp"

class StopWatch : protected Timer
{
  public:

    /**
     * Creates a stopwatch object.
     */
    StopWatch();

    /**
     * Starts the stopwatch.
     *
     * @param time
     *   time to be counted.
     */
    void Start(uint32_t time);

    /**
     * Stops the stopwatch.
     */
    void Stop();

    /**
     * @return
     *   Elapsed miliseconds since start.
     */
    uint32_t GetElapsed();

    /**
     * @return
     *   Remaining miliseconds to finish.
     */
    uint32_t GetRemaining();

    /**
     * @return
     *   True if the stopwatch is still running, false otherwise.
     */
    bool IsRunning();

  private:

    uint32_t _time;

    bool _running;
};

#endif /* COLMENA_TIME_STOPWATCH_HPP_ */

