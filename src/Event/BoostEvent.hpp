/**
 * File:   BoostEvent.hpp
 * Author: tfischer
 *
 * Colmena, a game engine, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef COLMENA_EVENT_EVENT_HPP_
#define COLMENA_EVENT_EVENT_HPP_

#include <boost/signals2/signal.hpp> // boost::signals2::signal
#include <boost/signals2/connection.hpp> // boost::signals2::connection, boost::signals2::scoped_connection

class NullType {};

typedef boost::signals2::connection ISubscription;
typedef boost::signals2::scoped_connection ScopedSubscription;

// ---------------------------------------------------------------------

template<
  class Param0 = NullType,
  class Param1 = NullType,
  class Param2 = NullType,
  class Param3 = NullType,
  class Param4 = NullType
> class Event;

// ---------------------------------------------------------------------

template <>
class Event<NullType, NullType, NullType, NullType, NullType>
{
  public:

    void Notify() const
    {
      signal_();
    }

    ISubscription Subscribe(boost::function<void()> callback)
    {
      return signal_.connect( callback );
    }

    template<class CLASS_T>
    ISubscription Subscribe(CLASS_T* class_instance, void (CLASS_T::*member_function_ptr)())
    {
      return signal_.connect(boost::bind(member_function_ptr, class_instance));
    }

  private:

    boost::signals2::signal<void ()> signal_;
};

// ---------------------------------------------------------------------

template <class P1_T>
class Event<P1_T, NullType, NullType, NullType, NullType>
{
  public:

    void Notify(P1_T p1) const
    {
      signal_(p1);
    }

    ISubscription Subscribe(boost::function<void(P1_T)> callback)
    {
      return signal_.connect( callback );
    }

    template<class CLASS_T>
    ISubscription Subscribe(CLASS_T* class_instance, void (CLASS_T::*member_function_ptr)(P1_T))
    {
      return signal_.connect(boost::bind(member_function_ptr, class_instance, _1));
    }

  private:

    boost::signals2::signal<void (P1_T)> signal_;
};

// ---------------------------------------------------------------------

template <class P1_T, class P2_T>
class Event<P1_T, P2_T, NullType, NullType, NullType>
{
  public:

    void Notify(P1_T p1, P2_T p2) const
    {
      signal_(p1, p2);
    }

    ISubscription Subscribe(boost::function<void(P1_T, P2_T)> callback)
    {
      return signal_.connect( callback );
    }

    template<class CLASS_T>
    ISubscription Subscribe(CLASS_T* class_instance, void (CLASS_T::*member_function_ptr)(P1_T, P2_T))
    {
      boost::function<void (P1_T, P2_T)> func = boost::bind(member_function_ptr, class_instance, _1, _2);
      return signal_.connect(func);
    }

  private:

    boost::signals2::signal<void (P1_T, P2_T)> signal_;
};

// ---------------------------------------------------------------------

template <class P1_T, class P2_T, class P3_T>
class Event<P1_T, P2_T, P3_T, NullType, NullType>
{
  public:

    void Notify(P1_T p1, P2_T p2, P3_T p3) const
    {
      signal_(p1, p2, p3);
    }

    ISubscription Subscribe(boost::function<void(P1_T, P2_T, P3_T)> callback)
    {
      return signal_.connect( callback );
    }

    template<class CLASS_T>
    ISubscription Subscribe(CLASS_T* class_instance, void (CLASS_T::*member_function_ptr)(P1_T, P2_T, P3_T))
    {
      return signal_.connect(boost::bind(member_function_ptr, class_instance, _1, _2, _3));
    }

  private:

    boost::signals2::signal<void (P1_T, P2_T, P3_T)> signal_;
};

// ---------------------------------------------------------------------

template <class P1_T, class P2_T, class P3_T, class P4_T>
class Event<P1_T, P2_T, P3_T, P4_T, NullType>
{
  public:

    void Notify(P1_T p1, P2_T p2, P3_T p3, P4_T p4) const
    {
      signal_(p1, p2, p3, p4);
    }

    ISubscription Subscribe(boost::function<void(P1_T, P2_T, P3_T, P4_T)> callback)
    {
      return signal_.connect( callback );
    }

    template<class CLASS_T>
    ISubscription Subscribe(CLASS_T* class_instance, void (CLASS_T::*member_function_ptr)(P1_T, P2_T, P3_T, P4_T))
    {
      return signal_.connect(boost::bind(member_function_ptr, class_instance, _1, _2, _3, _4));
    }

  private:

    boost::signals2::signal<void (P1_T, P2_T, P3_T, P4_T)> signal_;
};

#endif /* COLMENA_EVENT_EVENT_HPP_ */
