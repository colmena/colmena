/**
 * File:   CustomEvent.hpp
 * Author: tfischer
 *
 * Colmena, a game engine, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

Esta clase no anda. La idea es que cumpla con la misma interfaz que
BoostEvent.hpp, donde ISubscription no significa nada y
ScopedSubscription se desconecta del evento en el destructor

#ifndef COLMENA_EVENT_EVENT_HPP_
#define COLMENA_EVENT_EVENT_HPP_

#include <set>
#include <tr1/memory>

#define forall(it,X) for(typeof((X).begin()) it=(X).begin();it!=(X).end();it++)

class NullType {};

class ISubscription
{
  public:
    // ISubscription();
    // ISubscription(const ISubscription& subscription);
    typedef std::tr1::shared_ptr<ISubscription> Ptr;
    virtual ~ISubscription() {};
  //protected:
    //virtual bool Subscribe() = 0;
    //virtual bool Unsubscribe() = 0;
};

//~ class IScopedSubscription : ISubscription
//~ {
  //~ public:
    //~ // IScopedSubscription();
    //~ IScopedSubscription(const ISubscription& subscription);
    //~ virtual ~IScopedSubscription() {};
//~ };

// ---------------------------------------------------------------------

template<
  class Param0 = NullType,
  class Param1 = NullType,
  class Param2 = NullType,
  class Param3 = NullType,
  class Param4 = NullType
> class BaseFunctor;

template<
  class CLASS_T,
  class Param0 = NullType,
  class Param1 = NullType,
  class Param2 = NullType,
  class Param3 = NullType,
  class Param4 = NullType
> class Functor;

template<
  class Param0 = NullType,
  class Param1 = NullType,
  class Param2 = NullType,
  class Param3 = NullType,
  class Param4 = NullType
> class Event;

// ---------------------------------------------------------------------

template <>
class BaseFunctor<NullType, NullType, NullType, NullType, NullType>
{
  public:

    virtual void operator () () const = 0;

  protected:

    BaseFunctor() {}
};

template <class CLASS_T>
class Functor<CLASS_T, NullType, NullType, NullType, NullType, NullType>
    : public BaseFunctor<>
{
  public:

    typedef void (CLASS_T::*MemberFn)();

    Functor( CLASS_T* class_instance, MemberFn member_function_ptr )
    {
        _class_instance = class_instance;
        _member_function_ptr = member_function_ptr;
    }

    void operator () () const
    {
        (_class_instance->*_member_function_ptr)();
    }

  private:

    CLASS_T* _class_instance;

    MemberFn _member_function_ptr;
};

template <>
class Event<NullType, NullType, NullType, NullType, NullType>
{
  public:

    class Subscription : public ISubscription
    {
      public:

        Subscription(Event<>& event, BaseFunctor<>* callback)
          : _event(event), _callback(callback)
        {
          _event.Subscribe(_callback);
        }

        ~Subscription()
        {
          _event.Unsubscribe(_callback);
        }

      private:

        Event<>& _event;
        BaseFunctor<>* _callback;
    };

    ~Event()
    {
      forall(it,callbacks)
        delete (*it);
    }

    void Notify() const
    {
      forall(it,callbacks)
        (**it)();
    }

    template<class CLASS_T>
    typename ISubscription::Ptr Subscribe(CLASS_T* class_instance, void (CLASS_T::*member_function_ptr)())
    {
      BaseFunctor<>* callback = new Functor<CLASS_T>(class_instance, member_function_ptr);

      return typename ISubscription::Ptr( new Subscription(*this, callback) );
    }

    template<class CLASS_T>
    bool SubscribeHandler(CLASS_T* class_instance, void (CLASS_T::*member_function_ptr)())
    {
      BaseFunctor<>* callback = new Functor<CLASS_T>(class_instance, member_function_ptr);
      return Subscribe(callback);
    }

  private:

    bool Subscribe(BaseFunctor<>* callback)
    {
        if( callbacks.insert(callback).second == false )
          return false;
        return true;
    }

    bool Unsubscribe(BaseFunctor<>* callback)
    {
        if( callbacks.erase(callback) < 1)
            return false;
        return true;
    }

    std::set<BaseFunctor<>*> callbacks;
};

// ---------------------------------------------------------------------

template <class P1_T>
class BaseFunctor<P1_T, NullType, NullType, NullType, NullType>
{
  public:

    virtual void operator () (P1_T) const = 0;

  protected:

    BaseFunctor() {}
};

template <class CLASS_T, class P1_T>
class Functor<CLASS_T, P1_T, NullType, NullType, NullType, NullType>
    : public BaseFunctor<P1_T>
{
  public:

    typedef void (CLASS_T::*MemberFn)(P1_T);

    Functor( CLASS_T* class_instance, MemberFn member_function_ptr )
    {
        _class_instance = class_instance;
        _member_function_ptr = member_function_ptr;
    }

    void operator () (P1_T p1) const
    {
        (_class_instance->*_member_function_ptr)(p1);
    }

  private:

    CLASS_T* _class_instance;

    MemberFn _member_function_ptr;
};

template <class P1_T>
class Event<P1_T, NullType, NullType, NullType, NullType>
{
  public:

    class Subscription : public ISubscription
    {
      public:

        Subscription(Event<P1_T>& event, BaseFunctor<P1_T>* callback)
          : _event(event), _callback(callback)
        {
          _event.Subscribe(_callback);
        }

        ~Subscription()
        {
          _event.Unsubscribe(_callback);
        }

      private:

        Event<P1_T>& _event;
        BaseFunctor<P1_T>* _callback;
    };

    ~Event()
    {
      forall(it,callbacks)
        delete (*it);
    }

    void Notify(P1_T p1) const
    {
      forall(it,callbacks)
       (**it)(p1);
    }

    template<class CLASS_T>
    typename ISubscription::Ptr Subscribe(CLASS_T* class_instance, void (CLASS_T::*member_function_ptr)(P1_T))
    {
      BaseFunctor<P1_T>* callback = new Functor<CLASS_T, P1_T>(class_instance, member_function_ptr);

      return typename ISubscription::Ptr( new Subscription(*this, callback) );
    }

    template<class CLASS_T>
    bool SubscribeHandler(CLASS_T* class_instance, void (CLASS_T::*member_function_ptr)(P1_T))
    {
      BaseFunctor<P1_T>* callback = new Functor<CLASS_T, P1_T>(class_instance, member_function_ptr);
      return Subscribe(callback);
    }

  private:

    bool Subscribe(BaseFunctor<P1_T>* callback)
    {
      if( callbacks.insert(callback).second == false )
        return false;
      return true;
    }

    bool Unsubscribe(BaseFunctor<P1_T>* callback)
    {
      if( callbacks.erase(callback) < 1)
        return false;
      return true;
    }

    std::set<BaseFunctor<P1_T>*> callbacks;
};

// ---------------------------------------------------------------------

template <class P1_T, class P2_T>
class BaseFunctor<P1_T, P2_T, NullType, NullType, NullType>
{
  public:

    virtual void operator () (P1_T, P2_T) const = 0;

  protected:

    BaseFunctor() {}
};

template <class CLASS_T, class P1_T, class P2_T>
class Functor<CLASS_T, P1_T, P2_T, NullType, NullType, NullType>
    : public BaseFunctor<P1_T, P2_T>
{
  public:

    typedef void (CLASS_T::*MemberFn)(P1_T, P2_T);

    Functor( CLASS_T* class_instance, MemberFn member_function_ptr )
    {
      _class_instance = class_instance;
      _member_function_ptr = member_function_ptr;
    }

    void operator () (P1_T p1, P2_T p2) const
    {
      (_class_instance->*_member_function_ptr)(p1,p2);
    }

  private:

    CLASS_T* _class_instance;

    MemberFn _member_function_ptr;
};

template <class P1_T, class P2_T>
class Event<P1_T, P2_T, NullType, NullType, NullType>
{
  public:

    class Subscription : public ISubscription
    {
      public:

        Subscription(Event<P1_T, P2_T>& event, BaseFunctor<P1_T, P2_T>* callback)
          : _event(event), _callback(callback)
        {
          _event.Subscribe(_callback);
        }

        ~Subscription()
        {
          _event.Unsubscribe(_callback);
        }

      private:

        Event<P1_T, P2_T>& _event;
        BaseFunctor<P1_T, P2_T>* _callback;
    };

    ~Event()
    {
      forall(it,callbacks)
        delete (*it);
    }

    void Notify(P1_T p1, P2_T p2) const
    {
      forall(it,callbacks)
        (**it)(p1,p2);
    }

    template<class CLASS_T>
    typename ISubscription::Ptr Subscribe(CLASS_T* class_instance, void (CLASS_T::*member_function_ptr)(P1_T, P2_T))
    {
      BaseFunctor<P1_T, P2_T>* callback = new Functor<CLASS_T, P1_T, P2_T>(class_instance, member_function_ptr);

      return typename ISubscription::Ptr( new Subscription(*this, callback) );
    }

    template<class CLASS_T>
    bool SubscribeHandler(CLASS_T* class_instance, void (CLASS_T::*member_function_ptr)(P1_T, P2_T))
    {
      BaseFunctor<P1_T, P2_T>* callback = new Functor<CLASS_T, P1_T, P2_T>(class_instance, member_function_ptr);
      return Subscribe(callback);
    }

  private:

    bool Subscribe(BaseFunctor<P1_T,P2_T>* callback)
    {
      if( callbacks.insert(callback).second == false )
        return false;
      return true;
    }

    bool Unsubscribe(BaseFunctor<P1_T,P2_T>* callback)
    {
      if( callbacks.erase(callback) < 1)
        return false;
      return true;
    }

    std::set<BaseFunctor<P1_T,P2_T>*> callbacks;
};

// ---------------------------------------------------------------------

template <class P1_T, class P2_T, class P3_T>
class BaseFunctor<P1_T, P2_T, P3_T, NullType, NullType>
{
  public:

    virtual void operator () (P1_T, P2_T, P3_T) const = 0;

  protected:

    BaseFunctor() {}
};

template <class CLASS_T, class P1_T, class P2_T, class P3_T>
class Functor<CLASS_T, P1_T, P2_T, P3_T, NullType, NullType>
    : public BaseFunctor<P1_T, P2_T, P3_T>
{
  public:

    typedef void (CLASS_T::*MemberFn)(P1_T, P2_T, P3_T);

    Functor( CLASS_T* class_instance, MemberFn member_function_ptr )
    {
      _class_instance = class_instance;
      _member_function_ptr = member_function_ptr;
    }

    void operator () (P1_T p1, P2_T p2, P3_T p3) const
    {
      (_class_instance->*_member_function_ptr)(p1,p2,p3);
    }

  private:

    CLASS_T* _class_instance;

    MemberFn _member_function_ptr;
};

template <class P1_T, class P2_T, class P3_T>
class Event<P1_T, P2_T, P3_T, NullType, NullType>
{
  public:

    class Subscription : public ISubscription
    {
      public:

        Subscription(Event<P1_T, P2_T, P3_T>& event, BaseFunctor<P1_T, P2_T, P3_T>* callback)
          : _event(event), _callback(callback)
        {
          _event.Subscribe(_callback);
        }

        ~Subscription()
        {
          _event.Unsubscribe(_callback);
        }

      private:

        Event<P1_T, P2_T, P3_T>& _event;
        BaseFunctor<P1_T, P2_T, P3_T>* _callback;
    };

    ~Event()
    {
      forall(it,callbacks)
        delete (*it);
    }

    void Notify(P1_T p1, P2_T p2, P3_T p3) const
    {
      forall(it,callbacks)
        (**it)(p1,p2,p3);
    }

    template<class CLASS_T>
    typename ISubscription::Ptr Subscribe(CLASS_T* class_instance, void (CLASS_T::*member_function_ptr)(P1_T, P2_T, P3_T))
    {
      BaseFunctor<P1_T, P2_T, P3_T>* callback = new Functor<CLASS_T, P1_T, P2_T, P3_T>(class_instance, member_function_ptr);

      return typename ISubscription::Ptr( new Subscription(*this, callback) );
    }

    template<class CLASS_T>
    bool SubscribeHandler(CLASS_T* class_instance, void (CLASS_T::*member_function_ptr)(P1_T, P2_T, P3_T))
    {
      BaseFunctor<P1_T, P2_T, P3_T>* callback = new Functor<CLASS_T, P1_T, P2_T, P3_T>(class_instance, member_function_ptr);
      return Subscribe(callback);
    }

  private:

    bool Subscribe(BaseFunctor<P1_T,P2_T,P3_T>* callback)
    {
      if( callbacks.insert(callback).second == false )
        return false;
      return true;
    }

    bool Unsubscribe(BaseFunctor<P1_T,P2_T,P3_T>* callback)
    {
      if( callbacks.erase(callback) < 1)
        return false;
      return true;
    }

    std::set<BaseFunctor<P1_T,P2_T,P3_T>*> callbacks;
};

// ---------------------------------------------------------------------

template <class P1_T, class P2_T, class P3_T, class P4_T>
class BaseFunctor<P1_T, P2_T, P3_T, P4_T, NullType>
{
  public:

    virtual void operator () (P1_T, P2_T, P3_T, P4_T) const = 0;

  protected:

    BaseFunctor() {}
};

template <class CLASS_T, class P1_T, class P2_T, class P3_T, class P4_T>
class Functor<CLASS_T, P1_T, P2_T, P3_T, P4_T, NullType>
    : public BaseFunctor<P1_T, P2_T, P3_T, P4_T>
{
  public:

    typedef void (CLASS_T::*MemberFn)(P1_T, P2_T, P3_T, P4_T);

    Functor( CLASS_T* class_instance, MemberFn member_function_ptr )
    {
      _class_instance = class_instance;
      _member_function_ptr = member_function_ptr;
    }

    void operator () (P1_T p1, P2_T p2, P3_T p3, P4_T p4) const
    {
      (_class_instance->*_member_function_ptr)(p1,p2,p3,p4);
    }

  private:

    CLASS_T* _class_instance;

    MemberFn _member_function_ptr;
};

template <class P1_T, class P2_T, class P3_T, class P4_T>
class Event<P1_T, P2_T, P3_T, P4_T, NullType>
{
  public:

    class Subscription : public ISubscription
    {
      public:

        Subscription(Event<P1_T, P2_T, P3_T>& event, BaseFunctor<P1_T, P2_T, P3_T>* callback)
          : _event(event), _callback(callback)
        {
          _event.Subscribe(_callback);
        }

        ~Subscription()
        {
          _event.Unsubscribe(_callback);
        }

      private:

        Event<P1_T, P2_T, P3_T>& _event;
        BaseFunctor<P1_T, P2_T, P3_T>* _callback;
    };

    ~Event()
    {
      forall(it,callbacks)
        delete (*it);
    }

    void Notify(P1_T p1, P2_T p2, P3_T p3, P4_T p4) const
    {
      forall(it,callbacks)
        (**it)(p1,p2,p3,p4);
    }

    template<class CLASS_T>
    typename ISubscription::Ptr Subscribe(CLASS_T* class_instance, void (CLASS_T::*member_function_ptr)(P1_T, P2_T, P3_T, P4_T))
    {
      BaseFunctor<P1_T, P2_T, P3_T, P4_T>* callback = new Functor<CLASS_T, P1_T, P2_T, P3_T, P4_T>(class_instance, member_function_ptr);

      return typename ISubscription::Ptr( new Subscription(*this, callback) );
    }

    template<class CLASS_T>
    bool SubscribeHandler(CLASS_T* class_instance, void (CLASS_T::*member_function_ptr)(P1_T, P2_T, P3_T, P4_T))
    {
      BaseFunctor<P1_T, P2_T, P3_T, P4_T>* callback = new Functor<CLASS_T, P1_T, P2_T, P3_T, P4_T>(class_instance, member_function_ptr);
      return Subscribe(callback);
    }

  private:

    bool Subscribe(BaseFunctor<P1_T,P2_T,P3_T,P4_T>* callback)
    {
      if( callbacks.insert(callback).second == false )
        return false;
      return true;
    }

    bool Unsubscribe(BaseFunctor<P1_T,P2_T,P3_T,P4_T>* callback)
    {
      if( callbacks.erase(callback) < 1)
        return false;
      return true;
    }

    std::set<BaseFunctor<P1_T,P2_T,P3_T,P4_T>*> callbacks;
};

#endif /* COLMENA_EVENT_EVENT_HPP_ */
