/**
 * File:   ConsurrentEvent.hpp
 * Author: tfischer
 *
 * Colmena, a game engine, written in C++
 * Copyright (C) 2013  Thomas Fischer
 *
 * @brief Define Event classes, with the same interfaces,
 * but thread safe.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef COLMENA_EVENT_CONCURRENT_EVENT_HPP_
#define COLMENA_EVENT_CONCURRENT_EVENT_HPP_

#include <mutex> // std::mutex, std::lock_guard

#include "Event.hpp"

template<
  class Param0 = NullType,
  class Param1 = NullType,
  class Param2 = NullType,
  class Param3 = NullType,
  class Param4 = NullType
> class ConcurrentEvent;

// ---------------------------------------------------------------------

template <>
class ConcurrentEvent<NullType, NullType, NullType, NullType, NullType>
  : public Event<NullType, NullType, NullType, NullType, NullType>
{
  public:

    void Notify() const
    {
      std::lock_guard<std::mutex> lock( mutex_ );
      Event::Notify();
    }

    template<class CLASS_T>
    ISubscription Subscribe(CLASS_T* class_instance, void (CLASS_T::*member_function_ptr)())
    {
      std::lock_guard<std::mutex> lock( mutex_ );
      return Event::Subscribe(class_instance, member_function_ptr);
    }

  private:

    mutable std::mutex mutex_;
};

// ---------------------------------------------------------------------

template <class P1_T>
class ConcurrentEvent<P1_T, NullType, NullType, NullType, NullType>
  : public Event<P1_T, NullType, NullType, NullType, NullType>
{
  public:

    void Notify(P1_T p1) const
    {
      std::lock_guard<std::mutex> lock( mutex_ );
      Event<P1_T>::Notify( p1 );
    }

    template<class CLASS_T>
    ISubscription Subscribe(CLASS_T* class_instance, void (CLASS_T::*member_function_ptr)(P1_T))
    {
      std::lock_guard<std::mutex> lock( mutex_ );
      return Event<P1_T>::Subscribe(class_instance, member_function_ptr);
    }

  private:

    mutable std::mutex mutex_;
};

// ---------------------------------------------------------------------

template <class P1_T, class P2_T>
class ConcurrentEvent<P1_T, P2_T, NullType, NullType, NullType>
  : public Event<P1_T, P2_T, NullType, NullType, NullType>
{
  public:

    void Notify(P1_T p1, P2_T p2) const
    {
      std::lock_guard<std::mutex> lock( mutex_ );
      Event<P1_T, P2_T>::Notify( p1, p2 );
    }

    template<class CLASS_T>
    ISubscription Subscribe(CLASS_T* class_instance, void (CLASS_T::*member_function_ptr)(P1_T, P2_T))
    {
      std::lock_guard<std::mutex> lock( mutex_ );
      return Event<P1_T, P2_T>::Subscribe(class_instance, member_function_ptr);
    }

  private:

    mutable std::mutex mutex_;
};

// ---------------------------------------------------------------------

template <class P1_T, class P2_T, class P3_T>
class ConcurrentEvent<P1_T, P2_T, P3_T, NullType, NullType>
  : public Event<P1_T, P2_T, P3_T, NullType, NullType>
{
  public:

    void Notify(P1_T p1, P2_T p2, P3_T p3) const
    {
      std::lock_guard<std::mutex> lock( mutex_ );
      Event<P1_T, P2_T, P3_T>::Notify( p1, p2, p3 );
    }

    template<class CLASS_T>
    ISubscription Subscribe(CLASS_T* class_instance, void (CLASS_T::*member_function_ptr)(P1_T, P2_T, P3_T))
    {
      std::lock_guard<std::mutex> lock( mutex_ );
      return Event<P1_T, P2_T, P3_T>::Subscribe(class_instance, member_function_ptr);
    }

  private:

    mutable std::mutex mutex_;
};

// ---------------------------------------------------------------------

template <class P1_T, class P2_T, class P3_T, class P4_T>
class ConcurrentEvent<P1_T, P2_T, P3_T, P4_T, NullType>
  : public Event<P1_T, P2_T, P3_T, P4_T, NullType>
{
  public:

    void Notify(P1_T p1, P2_T p2, P3_T p3, P4_T p4) const
    {
      std::lock_guard<std::mutex> lock( mutex_ );
      Event<P1_T, P2_T, P3_T, P4_T>::Notify( p1, p2, p3, p4 );
    }

    template<class CLASS_T>
    ISubscription Subscribe(CLASS_T* class_instance, void (CLASS_T::*member_function_ptr)(P1_T, P2_T, P3_T, P4_T))
    {
      std::lock_guard<std::mutex> lock( mutex_ );
      return Event<P1_T, P2_T, P3_T, P4_T>::Subscribe(class_instance, member_function_ptr);
    }

  private:

    mutable std::mutex mutex_;
};

#endif // COLMENA_EVENT_CONCURRENT_EVENT_HPP_
